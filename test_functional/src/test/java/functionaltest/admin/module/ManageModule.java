package functionaltest.admin.module;
import functionaltest.FunctionalTest;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;

import static org.junit.Assert.assertEquals;

public abstract class ManageModule extends FunctionalTest {

    protected final String initialModule = "moduleTest";
    protected final String initialEnseignant = "minette paul";
    protected final String modifiedModule = "moduleTest";
    protected final String modifiedEnseignant = "moduleTest";

    protected void createModule(String url) {
        driver.get(url);
        driver.findElement(By.id("nom")).sendKeys(this.initialModule);
        new Select(driver.findElement(By.id("ensRef"))).selectByVisibleText(this.initialEnseignant);
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Enseignant Référent'])[1]/following::option[2]")).click();
        driver.findElement(By.id("creer")).click();
        try {
            assertEquals("Module créée avec succès !", super.driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }

    protected void deleteModule(String search) {
        driver.get(super.urlRoot + "controllers/module.php");
        driver.findElement(By.name("recherche")).sendKeys(search);
        driver.findElement(By.name("recherche")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Référent'])[1]/following::td[1]")).click();
        driver.findElement(By.name("supprimer")).click();
        try {
            assertEquals("Module supprimé avec succès !", super.driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }
}

