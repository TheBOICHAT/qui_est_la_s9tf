package functionaltest.admin.module;

import functionaltest.FunctionalTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class CreateEmptyModuleTest extends FunctionalTest {
    @Test
    public void testAdminCreateEmptyModule() {
        driver.get(super.urlRoot + "controllers/module.php");
        driver.findElement(By.id("creer")).click();
        try {
            Assert.assertEquals("Veuillez renseigner le libellé de module.", driver.findElement(By.id("error_nom")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
        try {
            Assert.assertEquals("Veuillez choisir un enseignant réferent", super.driver.findElement(By.id("error_ensref")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }
}
