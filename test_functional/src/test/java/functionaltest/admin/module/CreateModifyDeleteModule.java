package functionaltest.admin.module;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static org.junit.Assert.assertEquals;

public class CreateModifyDeleteModule extends ManageModule {

    @Override
    public void setUp() {
        super.setUp();
        super.createModule(super.urlRoot + "controllers/module.php");
    }


    @Test
    public void testAdminSearchAndModifyModule() {
        driver.get(super.urlRoot + "controllers/module.php");
        driver.findElement(By.name("recherche")).sendKeys(super.initialModule);
        super.driver.findElement(By.name("recherche")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Référent'])[1]/following::td[1]")).click();
        driver.findElement(By.id("nom")).sendKeys(this.modifiedModule);
        driver.findElement(By.name("modifier")).click();
        try {
            assertEquals("Module modifié avec succès !", driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @Override
    public void tearDown() {
        super.deleteModule(super.modifiedModule);
        super.tearDown();
    }
}
