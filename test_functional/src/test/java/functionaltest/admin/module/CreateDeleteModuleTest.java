package functionaltest.admin.module;
import org.junit.Test;

public class CreateDeleteModuleTest extends ManageModule {

    @Test
    public void testAdminCreateAndDeleteModule() {
        super.createModule(super.urlRoot + "controllers/module.php");
        super.deleteModule(super.initialModule);
    }
}