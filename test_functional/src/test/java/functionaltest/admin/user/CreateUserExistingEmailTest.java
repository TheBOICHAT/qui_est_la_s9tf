package functionaltest.admin.user;

import functionaltest.admin.ManageUser;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;

public class CreateUserExistingEmailTest extends ManageUser {

    @Override
    public void setUp() {
        super.setUp();
        super.createUser(super.urlRoot + "controllers/utilisateur.php");
    }

    @Test
    public void testAdminCreateUserExistingEmail() {
        super.driver.findElement(By.id("nom")).sendKeys(super.initialNomPrenom);
        super.driver.findElement(By.id("prenom")).sendKeys(super.initialNomPrenom);
        super.driver.findElement(By.id("inputEmail")).sendKeys(super.initialEmail);
        super.driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Rôle'])[1]/following::label[1]")).click();
        super.driver.findElement(By.name("creer")).click();
        try {
            assertEquals("Un utilisateur existe déjà pour cette adresse email.", super.driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }

    @Override
    public void tearDown() {
        super.deleteUser(super.initialNomPrenom);
        super.tearDown();
    }
}
