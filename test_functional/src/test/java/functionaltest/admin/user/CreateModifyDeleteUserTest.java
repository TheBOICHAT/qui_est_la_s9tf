package functionaltest.admin.user;

import functionaltest.admin.ManageUser;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static org.junit.Assert.assertEquals;

public class CreateModifyDeleteUserTest extends ManageUser {

    @Override
    public void setUp() {
        super.setUp();
        super.createUser(super.urlRoot + "controllers/utilisateur.php");
    }

    @Test
    public void testAdminSearchAndModifyUser() {
        driver.findElement(By.name("recherche")).sendKeys(super.initialNomPrenom);
        driver.findElement(By.name("recherche")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Rôle'])[1]/following::td[1]")).click();
        driver.findElement(By.id("nom")).sendKeys(super.modifiedNomPrenom);
        driver.findElement(By.id("prenom")).sendKeys(super.modifiedNomPrenom);
        driver.findElement(By.id("inputEmail")).sendKeys(super.modifiedEmail);
        driver.findElement(By.id("rad2")).click();
        driver.findElement(By.name("modifier")).click();
        try {
            assertEquals("Informations modifiées avec succès !", driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    @Override
    public void tearDown() {
        super.deleteUser(super.modifiedNomPrenom);
        super.tearDown();
    }
}
