package functionaltest.admin.user;

import functionaltest.FunctionalTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class CreateEmptyUserTest extends FunctionalTest {
    @Test
    public void testAdminCreateEmptyUser() {
        super.driver.get(super.urlRoot + "controllers/utilisateur.php");
        super.driver.findElement(By.name("creer")).click();
        try {
            Assert.assertEquals("Veuillez renseigner le nom.", super.driver.findElement(By.id("error_nom")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
        try {
            Assert.assertEquals("Veuillez renseigner le prénom.", super.driver.findElement(By.id("error_prenom")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
        try {
            Assert.assertEquals("Veuillez renseigner l'adresse email.", super.driver.findElement(By.id("error_email")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
        try {
            Assert.assertEquals("Veuillez choisir un rôle.", super.driver.findElement(By.id("error_role")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }
}
