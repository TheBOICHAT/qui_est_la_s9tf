package functionaltest.admin.user;

import functionaltest.admin.ManageUser;
import org.junit.Test;

public class CreateDeleteUserTest extends ManageUser {

    @Test
    public void testAdminCreateAndDeleteUser() {
        super.createUser(super.urlRoot + "controllers/utilisateur.php");
        super.deleteUser(super.initialNomPrenom);
    }
}
