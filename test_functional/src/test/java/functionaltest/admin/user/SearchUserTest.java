package functionaltest.admin.user;

import functionaltest.admin.ManageUser;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static org.junit.Assert.assertEquals;

public class SearchUserTest extends ManageUser {

    @Override
    public void setUp() {
        super.setUp();
        super.createUser(super.urlRoot + "controllers/utilisateur.php");
    }

    @Test
    public void testAdminSearchAndFindUser() {
        super.driver.findElement(By.name("recherche")).sendKeys(super.initialNomPrenom);
        super.driver.findElement(By.name("recherche")).sendKeys(Keys.ENTER);
        super.driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Rôle'])[1]/following::td[1]")).click();
        try {
            assertEquals(super.initialNomPrenom, driver.findElement(By.id("nom")).getAttribute("value"));
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }

    @Override
    public void tearDown() {
        super.deleteUser(super.initialNomPrenom);
        super.tearDown();
    }
}
