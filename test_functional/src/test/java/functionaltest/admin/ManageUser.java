package functionaltest.admin;

import functionaltest.FunctionalTest;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static org.junit.Assert.assertEquals;

public abstract class ManageUser extends FunctionalTest {

    protected final String initialNomPrenom = "initial";
    protected final String initialEmail = "ini@test.ts";
    protected final String modifiedNomPrenom = "modified";
    protected final String modifiedEmail = "mod@test.ts";

    protected void createUser(String url) {
        super.driver.get(url);
        super.driver.findElement(By.id("nom")).sendKeys(this.initialNomPrenom);
        super.driver.findElement(By.id("prenom")).sendKeys(this.initialNomPrenom);
        super.driver.findElement(By.id("inputEmail")).sendKeys(this.initialEmail);
        super.driver.findElement(By.id("rad2")).click();
        super.driver.findElement(By.name("creer")).click();
        try {
            assertEquals("Utilisateur créée avec succès !", super.driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }

    protected void deleteUser(String search) {
        super.driver.findElement(By.name("recherche")).sendKeys(search);
        super.driver.findElement(By.name("recherche")).sendKeys(Keys.ENTER);
        super.driver.findElement(By.xpath("(.//*[normalize-space(text()) and normalize-space(.)='Rôle'])[1]/following::td[1]")).click();
        super.driver.findElement(By.name("supprimer")).click();
        try {
            assertEquals("Utilisateur supprimé avec succès !", super.driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }
}
