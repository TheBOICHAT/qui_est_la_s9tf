package functionaltest.admin.search;

import functionaltest.FunctionalTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import static org.junit.Assert.assertEquals;

public class NoResultSearchTest extends FunctionalTest {
    @Test
    public void testAdminSearchNoResult() {
        driver.get(this.urlRoot + "controllers/recherche.php");
        driver.findElement(By.name("recherche")).sendKeys("pouet_haha_bonjour");
        driver.findElement(By.name("recherche")).sendKeys(Keys.ENTER);
        try {
            assertEquals("Aucun résultat pour la recherche : \"pouet_haha_bonjour\"", driver.findElement(By.id("status_msg")).getText());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }
}
