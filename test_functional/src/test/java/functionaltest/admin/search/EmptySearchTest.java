package functionaltest.admin.search;

import functionaltest.FunctionalTest;
import org.junit.Test;
import org.openqa.selenium.By;

import static junit.framework.TestCase.assertEquals;

public class EmptySearchTest extends FunctionalTest {
    @Test
    public void testAdminSearchEmpty() {
        super.driver.get(super.urlRoot + "/controllers/recherche.php");
        try {
            assertEquals("Veuillez procéder à une recherche !", super.driver.findElement(By.id("msg_empty_search")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }
}
