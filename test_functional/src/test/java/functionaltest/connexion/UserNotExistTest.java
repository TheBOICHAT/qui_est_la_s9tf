package functionaltest.connexion;

import functionaltest.FunctionalTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class UserNotExistTest extends FunctionalTest {

    @Test
    public void testNotExistingUser() {
        super.driver.get(super.urlRoot+"controllers/connexion.php");
        super.driver.findElement(By.name("idEmail")).sendKeys("bonjour@ceciestuntest.ts");
        super.driver.findElement(By.id("idMotDePasse")).sendKeys("pouet");
        super.driver.findElement(By.name("submit")).click();
        try {
            Assert.assertEquals("Utilisteur Inconnu renseigner un nouveau email", super.driver.findElement(By.id("error_mail")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }
}
