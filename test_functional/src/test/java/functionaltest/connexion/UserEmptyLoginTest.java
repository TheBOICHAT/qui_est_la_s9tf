package functionaltest.connexion;

import functionaltest.FunctionalTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class UserEmptyLoginTest extends FunctionalTest {
    @Test
    public void testEmptyLogin() {
        super.driver.get(super.urlRoot + "controllers/connexion.php");
        super.driver.findElement(By.id("idMotDePasse")).sendKeys("yassine");
        super.driver.findElement(By.name("submit")).click();
        try {
            Assert.assertEquals("Veuillez renseigner votre Email.", super.driver.findElement(By.id("error_login")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }

    }


}
