package functionaltest.connexion;

import functionaltest.FunctionalTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class UserErrorPasswordTest extends FunctionalTest {
    @Test
    public void testErrorPassword() {
        super.driver.get(super.urlRoot + "controllers/connexion.php");
        super.driver.findElement(By.id("idEmail")).sendKeys("testCO@co.ts");
        super.driver.findElement(By.id("idMotDePasse")).sendKeys("lestestcestchiant");
        super.driver.findElement(By.name("submit")).click();
        try {
            Assert.assertEquals("Votre mot de passe est incorect !", super.driver.findElement(By.id("error_mdpEr")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }
}
