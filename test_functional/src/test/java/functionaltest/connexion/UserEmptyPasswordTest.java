package functionaltest.connexion;

import functionaltest.FunctionalTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

public class UserEmptyPasswordTest extends FunctionalTest {
    @Test
    public void testEmptyPassword() {
        super.driver.get(super.urlRoot + "controllers/connexion.php");
        super.driver.findElement(By.id("idEmail")).sendKeys("yas.lou@gm.fr");
        super.driver.findElement(By.name("submit")).click();
        try {
            Assert.assertEquals("Veuillez renseigner votre mot de passe.", super.driver.findElement(By.id("error_mdp")).getText());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }

    }

}
