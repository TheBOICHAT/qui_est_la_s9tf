package functionaltest.connexion;

import functionaltest.FunctionalTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;


public class ExistingUserTest extends FunctionalTest {

    @Test
    public void testExisting() {
        super.driver.get(super.urlRoot + "controllers/connexion.php");
        super.driver.findElement(By.id("idEmail")).sendKeys("testCO@co.ts");
        driver.findElement(By.id("idMotDePasse")).sendKeys("pouet");
        driver.findElement(By.name("submit")).click();

        try {
            Assert.assertEquals("Utilisateur", super.driver.getTitle());
        } catch (Error e) {
            super.verificationErrors.append(e.toString());
        }
    }
}
