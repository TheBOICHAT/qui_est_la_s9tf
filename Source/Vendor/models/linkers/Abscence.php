<?php
namespace Vendor\models\linkers;

class Abscence
{
    private $idAbscence;
    private $date;
    private $motif;
    private $commentaire;
    private $status;  //justifié ou non
    private $idModule;
    private $idEnseignant;



    /**
     * Abscence constructor.
     * @param $date
     * @param $motif
     */

    public function __construct($motif, $date, $commentaire="")
    {
        $this->motif = $motif;
        $this->commentaire=$commentaire;
        $this->date=$date;

    }


    /**
     * @return mixed
     */
    public function getIdModule()
    {
        return $this->idModule;
    }

    /**
     * @param mixed $idModule
     */
    public function setIdModule($idModule)
    {
        $this->idModule = $idModule;
    }

    /**
     * @return mixed
     */
    public function getIdEnseignant()
    {
        return $this->idEnseignant;
    }

    /**
     * @param mixed $idEnseignant
     */
    public function setIdEnseignant($idEnseignant)
    {
        $this->idEnseignant = $idEnseignant;
    }

    /**
     * @return mixed
     */
    public function getIdEtudiant()
    {
        return $this->idEtudiant;
    }

    /**
     * @param mixed $idEtudiant
     */
    public function setIdEtudiant($idEtudiant)
    {
        $this->idEtudiant = $idEtudiant;
    }
    private $idEtudiant;



    /**
     * @return int
     */
    public function getIdAbscence()
    {
        return $this->idAbscence;
    }

    /**
     * @param mixed $idAbscence
     */
    public function setIdAbscence($idAbscence)
    {
        $this->idAbscence = $idAbscence;
    }

    /**
     * @return date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param date $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return
     */
    public function getMotif()
    {
        return $this->motif;
    }

    /**
     * @param  $motif
     */

    public function setMotif($motif)
    {
        $this->motif = $motif;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */

    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    }