<?php

namespace Vendor\models\linkers;
class Module
{
    private $idModule;
    private $libelle;
    private $enseignant_referant;
    private $ens_referant_ref;

    public function __construct($lib ="", $ens=-1) {
        $this->libelle = $lib;
        $this->enseignant_referant = $ens;
    }

    public  function to_string() {
        return "Module nb : ".$this->getIdModule()." libelle : ".$this->getLibelle()." referant : ".$this->getEnseignantReferant();
    }

    /**
     * @return mixed
     */
    public function getIdModule()
    {
        return $this->idModule;
    }

    /**
     * @param mixed $idModule
     */
    public function setIdModule($idModule)
    {
        $this->idModule = $idModule;
    }

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getEnseignantReferant()
    {
        return $this->enseignant_referant;
    }

    /**
     * @param mixed $enseignant_referant
     */
    public function setEnseignantReferant($enseignant_referant)
    {
        $this->enseignant_referant = $enseignant_referant;
    }

    /**
     * @return mixed
     */
    public function getEnsReferantRef()
    {
        return $this->ens_referant_ref;
    }

    /**
     * @param mixed $ens_referant_ref
     */
    public function setEnsReferantRef($ens_referant_ref)
    {
        $this->ens_referant_ref = $ens_referant_ref;
    }
}