<?php
namespace Vendor\models\linkers;

class Utilisateur
{
    private $nom;
    private $prenom;
    private $mail;
    private $motDePass;
    private $id;
    private $role;

    /**
     * Utilisateur constructor.
     * @param $nom
     * @param $prenom
     * @param $mail
     * @param int $idRole
     */

    public function __construct($nom="", $prenom="", $role=0)
    {
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->role = EnumRole::toDB($role);
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param mixed $mail
     */
    public function setMail($mail)

    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getMotDePass()
    {
        return $this->motDePass;
    }

    /**
     * @param mixed $motDePass
     */
    public function setMotDePass($motDePass)
    {
        $this->motDePass = $motDePass;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRoleDB()
    {
        return $this->role;
    }

    /**
     * @return mixed
     */
    public function getRoleModel()
    {
        return EnumRole::toModel($this->role);
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = EnumRole::toDB($role);
    }
}
