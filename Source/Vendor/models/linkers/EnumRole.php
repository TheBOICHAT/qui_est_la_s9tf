<?php
namespace Vendor\models\linkers;

class EnumRole
{
    const __default = self::Etudiant;

    // Valeur modele pour les controlleurs
    const Administrateur = 1;
    const Etudiant = 2;
    const Enseignant = 3;
    const Personnel = 4;

    // Valeur pour la base de données.
    const AdministrateurDB = 'admin';
    const EtudiantDB = 'etudiant';
    const EnseignantDB = 'enseignant';
    const PersonnelDB = 'pers_admin';

    // Libelle.
    const AdministrateurLib = 'Administrateur';
    const EtudiantLib = 'Étudiant';
    const EnseignantLib = 'Enseignant';
    const PersonnelLib = 'Personnel administratif';

    /**
     * Conversion des valeurs de rôles utilisées dans
     * l'application vers celles de la base de données
     * @param $roleModel int valeur d'un rôle dans l'application
     * @return string valeur équivalente du rôle dans la base de données
     */
    public static function toDB($roleModel)
    {
        switch($roleModel) {
            case EnumRole::Administrateur : return self::AdministrateurDB;
            case EnumRole::Etudiant : return self::EtudiantDB;
            case EnumRole::Enseignant : return self::EnseignantDB;
            case EnumRole::Personnel : return self::PersonnelDB;
            default : return $roleModel;
        }
    }

    /**
     * Conversion des valeurs de rôles utilisées dans
     * dans la base de données vers l'application
     * @param $roleDB string valeur d'un rôle dans la base de données
     * @return int valeur équivalente du rôle dans l'application
     */
    public static function toModel($roleDB)
    {
        switch($roleDB) {
            case EnumRole::AdministrateurDB : return self::Administrateur;
            case EnumRole::EtudiantDB : return self::Etudiant;
            case EnumRole::EnseignantDB : return self::Enseignant;
            case EnumRole::PersonnelDB : return self::Personnel;
            default : return $roleDB;
        }
    }

    /**
     * Retourne le libellé lié au rôle
     * @param $role mixed rôle à traiter
     * @return string libelle du rôle
     */
    public static function toString($role)
    {
        if ($role === EnumRole::Administrateur || $role === EnumRole::AdministrateurDB) {
            return EnumRole::AdministrateurLib;
        } else if ($role === EnumRole::Etudiant || $role === EnumRole::EtudiantDB) {
            return EnumRole::EtudiantLib;
        } else if ($role === EnumRole::Enseignant || $role === EnumRole::EnseignantDB) {
            return EnumRole::EnseignantLib;
        } else if ($role === EnumRole::Personnel || $role === EnumRole::PersonnelDB) {
            return EnumRole::PersonnelLib;
        } else {
            return '?';
        }
    }
}