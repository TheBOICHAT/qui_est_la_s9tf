<?php
namespace Vendor\models\controllers;
use Vendor\models\linkers\EnumRole;
session_start();

/**
 * Class ControlConnexion
 * @package Vendor\models\controllers
 */
class ControlConnexion implements Control
{
    private $login;
    private $password;
    private $userDao;
    private $errors;
    private $view;
    private $user = null;
    private $pathViews;
    private $chekConx;

    /**
     * ControlConnexion constructor.
     * @param $path string racine des vues
     * @param $login string represente l'email de l'utilisateur
     * @param $password string represente le mot de passe utilisateur
     * @param $userDao UtilisateurDao intéraction avec la base de données
     * @param $view string vue a afficher
     * @param $chekConx classe permettant de verifier les donees du formulaire
     */
    public function __construct($path, $login, $password, $userDao , $view, $chekConx)
    {
        $this->pathViews = $path;
        $this->login = htmlspecialchars($login);
        $this->password = $password;
        $this->view = $view;
        $this->userDao = $userDao;
        $this->chekConx = $chekConx;
        $this->errors=array();

    }
    /**
     * @return l'utilisateur courant
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @return la vue a afficher
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @return string
     */
    public function getPathView()
    {
        return $this->pathViews;
    }
    /**
     * la redirection de l'utilisateur vers son espace
     */
    public function checkUser()
    {
        if($this->isValidUser())
        {
            header("Location: " .$this->redirect());
            return true;
        }
        return false;
    }
    /**
     * @return true si l'utilisateur est enregistre dans la session false sinon
     */
    public function isValidUser()
    {
        if($this->findUser()) {
            $this->storeUserSession($this->user);
            return true;
        }
        else
        {
            return false;
        }
    }
    /**
     * @return string l'email de l'utilisateur
     */
    public function getLogin()
    {
        return $this->login;
    }
    /**
     * @return string mot de passe de l'utilisateur entree
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * @return true si l'utilisateur existe dans la base de donne false sinon
     */
    public function findUser()
    {
        $this->errors = $this->chekConx->checkConxErrors($this->login, $this->password);
        if($this->is_array_empty($this->errors))
        {
            return $this->verifyEmail();
        }
        return false;
    }
    /**
     * @return string la page de redirection de l'utilisateur
     */
    public function redirect()
    {
        switch($this->user->getRoleDB())
        {
            case EnumRole::AdministrateurDB : $page = "utilisateur.php"; break;
            case EnumRole::PersonnelDB    : $page = ""; break;
            case EnumRole::EnseignantDB   : $page =  ""; break;
        }
        return $page;
    }
    /**
     * @param $user utilisateur a enregistrer dans la session
     */
    private function storeUserSession($user)
    {
        $_SESSION['user'] = $user;
    }

    /**
     * @return la vue a afficher
     */
    public function renderView()
    {
        return require_once($this->pathViews.$this->view);
    }
    /**
     * verifie si le tableu est vide
     * @param $array le tableau a verifier
     * @return bool true si le tableau est vide sinon false
     */
    public function is_array_empty($array)
    {
        foreach ($array as $key => $value) {
            if(!empty($key) && !empty($value))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * la fonction verifie l'existance de l'email et la validite du mot de passe
     * @return bool true mot de passe est valide sinon mot de passe incorect
     */
    public function verifyEmail()
    {
        $this->user = $this->userDao->findByEmail($this->login);
        if (is_null($this->user)) {
            $this->errors['verifyMail'] = "Utilisteur Inconnu renseigner un nouveau email";
            return false;
        }else
        {
            $match = password_verify($this->password, $this->user->getMotDePass());
            if ($match)
            {
                $this->errors['connecte'] = "Utilisteur Connecte";
                return true;
            } else
            {
                $this->errors['verifyMDP'] = "Votre mot de passe est incorect !";
                return false;
            }
        }
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

}
