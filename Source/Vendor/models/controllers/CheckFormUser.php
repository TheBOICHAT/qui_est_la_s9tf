<?php
namespace Vendor\models\controllers;

use Vendor\models\linkers\EnumRole;

class CheckFormUser
{
    /**
     * Vérification des erreurs liées au remplissage du formulaire.
     * @param $last_name string nom de l'utilisateur
     * @param $first_name string prenom de l'utilisateur
     * @param $email string email de l'utilisateur
     * @param $role int rôle de l'utilisateur
     * @return array erreurs liées au formulaire
     */
    public function checkFormErrors($last_name, $first_name, $email, $role)
    {
        $errors = array();
        if (empty($last_name)) {
            $errors['nom'] = "Veuillez renseigner le nom.";
        }
        if (empty($first_name)) {
            $errors['prenom'] = "Veuillez renseigner le prénom.";
        }
        if (empty($email)) {
            $errors['email'] = "Veuillez renseigner l'adresse email.";
        }
        if (empty($role)) {
            $errors['role'] = "Veuillez choisir un rôle.";
        }
        if (!isset($errors['role']) and ($role < EnumRole::Administrateur or $role > EnumRole::Personnel)) {
            $errors['role'] = "Le rôle choisit est inconnu.";
        }
        return $errors;
    }

    /**
     * Vérification des erreurs liées à l'identificateur de l'utilisateur à modifier.
     * @param $id int identifiant d'un utilisateur déjà existant
     * @return array erreurs liées aà l'identificateur
     */
    public function checkIdErrors($id)
    {
        $status = array();
        if ($id === -1) {
            $status['success'] = false;
            $status['msg'] = "Identifiant utilisateur invalide.";
        }
        return $status;
    }
}