<?php

namespace Vendor\models\controllers;

use Vendor\models\dao\EchecDeConnexionDBException;
use Vendor\models\linkers\Module;
use Vendor\models\linkers\Utilisateur;


/** Class ControlUser Gére l'intéraction avec le formulaire des modules. */
class ControlModule
{


    private $moduleDao;
    private $utilisateurDao;
    private $formChecker;
    private $pathViews;
    private $view;


    // Informations liées au formulaire.
    private $nom;
    private $ensRef;
    private $id;

    private $errors;
    private $status;
    private $is_modifing;
    private $nomRef;

    /**
     * Définit les valeurs des attributs.
     * @param $view string vue à afficher
     * @param $nom string nom
     * @param $ensRef int enseignantReferent
     * @param $id int identifiant d'un module déjà existant.
     */
    public function __construct($moduleDao,$utilisateurDao,$formChecker, $pathViews, $view, $dataPost)
    {

        $this->moduleDao = $moduleDao;
        $this->utilisateurDao = $utilisateurDao;
        $this->formChecker = $formChecker;
        $this->view = $view;
        $this->pathViews = $pathViews;

        $this->nom = (isset($dataPost['nom'])) ? htmlspecialchars($dataPost['nom']) : null;
        $this->ensRef = (isset($dataPost['ensRef'])) ? $this->defineValue($dataPost['ensRef']) : null;
        $this->id = (isset($dataPost['id'])) ? $this->defineValue($dataPost['id']) : null;

        if (!empty($this->ensRef)) {
            $utilisateur = $this->utilisateurDao->getUserById($this->ensRef);
            if (!is_null($utilisateur)) {
                $this->nomRef = $utilisateur->getNom()." ".$utilisateur->getPrenom();
            }
        }

        $this->errors = array();
        $this->ens_refss = array();
        $this->status = array();
        $this->is_modifing = false;

    }


    /**
     * Initialise les entiers correctement
     * de façon à détecter les différentes erreurs.
     * @param $to_process int donnée à traiter
     * @return int donnée traitée
     */
    private function defineValue($to_process)
    {
        if (!empty($to_process)) {
            if (is_numeric($to_process)) {
                // Valeur choisi et bien formé
                return (int) $to_process;
            } else {
                // Valeur choisi et mal formé
                return -1;
            }
        }
        // Aucune valeur choisi
        return $to_process;
    }

    /**
     * Remet à zéro les valeurs affichées dans le formulaire.
     */
    public function resetData()
    {
        $this->nom = '';
        $this->ensRef = -1;
        $this->id = -1;
    }
    /**
     * Création de module dans la base de données.
     */
    public function requestCreation()
    {
        $this->errors = $this->formChecker->checkFormErrors($this->nom, $this->ensRef);

        if (empty($this->errors)) {
            $currentModule = $this->buildCurrentModule();
            $is_module_created = true;
            $msg = "Module créée avec succès !";

            try {
                $this->moduleDao->add($currentModule);
            } catch (EchecDeConnexionDBException $e) {
                $is_module_created = false;
                $msg = "Erreur lors de la création du Module";
            }

            $this->status['success'] = $is_module_created;
            $this->status['msg'] = $msg;

            if ($is_module_created) {
                $this->resetData();
            }
        }

    }

    public function getAllEnseignantsReferents()
    {

        return $this->utilisateurDao->getAllEnseignantRef();

    }


    /**
     * Récupération des informations d'un module déjà existant.
     */
    public function fetchInformation()
    {
        $this->status = $this->formChecker->checkIdErrors($this->id);
        if (empty($this->status)) {
            $this->is_modifing = true;
            try {
                $module = $this->moduleDao->getById($this->id);

                $this->nom = $module->getLibelle();
                $this->ensRef = $module->getEnseignantReferant();
                $utilisateur = $this->utilisateurDao->getUserById($this->ensRef);
                $this->nomRef = $utilisateur->getNom()." ".$utilisateur->getPrenom();

            } catch (EchecDeConnexionDBException $e) {
                $this->status['success'] = false;
                $this->status['msg'] = "Erreur lors de l'obtention des informations du module (".$e->getMessage().").";
            }
        }
    }


    /**
     * Modification des informations d'un module déjà existant.
     */
    public function requestModification()
    {
        $this->errors = $this->formChecker->checkFormErrors($this->nom, $this->ensRef);
        $this->status = $this->formChecker->checkIdErrors($this->id);

        if (empty($this->errors) and empty($this->status)) {
            $is_module_created = true;
            $currentModule = $this->buildCurrentModule();
            $msg = "Module modifié avec succès !";
            try {
                $this->moduleDao->update($currentModule);
            }
            catch(EchecDeConnexionDBException $e) {
                $is_module_created = false;
                $msg = "Erreur lors de la modéfication du module !";
            }
            $this->status['success'] = $is_module_created;
            $this->status['msg'] = $msg;
        }
        $this->is_modifing = true;
    }

    /**
     * Supprime un Module déjà existant.
     */
    public function requestDeletion()
    {
        $this->status = $this->formChecker->checkIdErrors($this->id);

        if (empty($this->errors) and empty($this->status)) {
            $currentModule = $this->buildCurrentModule();
            $is_module_deleted = true;
            $msg = "Module supprimé avec succès !";
            try {
                $this->moduleDao->delete($currentModule);
            } catch (EchecDeConnexionDBException $e) {
                $is_module_deleted = false;
                $msg = "Erreur lors de la suppression de module";
            }
            $this->status['success'] = $is_module_deleted;
            $this->status['msg'] = $msg;
        }
        $this->resetData();

    }

    /**
     * Affiche la vue de module.
     * @return mixed donnée composant la vue
     */
    public function renderView()
    {
        $this->ens_refss = $this->getAllEnseignantsReferents() ;
        return require_once($this->pathViews.$this->view);

    }
    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }


    private function buildCurrentModule()
    {
        $module= new Module();
        $module->setLibelle($this->nom);
        $module->setEnseignantReferant($this->ensRef);
        $module->setIdModule($this->id);
        return $module;
    }
    /**
     * @return string
     */
    public function getPathViews()
    {
        return $this->pathViews;
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }


    /**
     * @return int
     */
    public function getEnseignantReferant()
    {
        return $this->ensRef;
    }

    /**
     * @return int
     */
    public function getIdModule()
    {
        return $this->id;
    }



    /**
     * @return array
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isModifing()
    {
        return $this->is_modifing;
    }



}

