<?php
namespace Vendor\models\controllers;

use Vendor\models\dao\ConnexionToServer;
use Vendor\models\dao\DaoFactory;
use Vendor\models\dao\UtilisateurDao;
use Vendor\models\dao\ParsingConfigFileException;

/**
 * Class ConstructUser
 * @package Vendor\models\controllers
 */
class ConstructUser implements Control {

	private $factory;
	private $configuration;
	private $conexion;
	private $userDao;
	private $pathViews;
    private $control = null;

    /**
     * ConstructUser constructor.
     * @param $path string racine des vues
     * @param $configuration le chemin de la configuration de la base de donnee
     */
    public function __construct($path , $configuration)
	{
	        $this->pathViews = $path;
			$this->configuration = $configuration;
			try
            {
               $this->conexion = new ConnexionToServer($configuration);
            }
            catch (ParsingConfigFileException $e)
            {
                $this->control = new ControlError(
                    $this->pathViews,
                    'general/erreur.php',
                    'Le fichier de configuration à la base de données n\'est pas valide.',
                    'Fichier : ' . $this->configuration
                );
            }
            $this->factory = new DaoFactory($this->conexion);
            $this->userDao = new UtilisateurDao($this->factory);
	}

    /**
     * @return DaoFactory
     */
    public function getFactory()
	{
		return $this->factory;
	}

    /**
     * @return ConnexionToServer
     */
    public function getConnexion()
	{
		return $this->conexion;
	}

    /**
     * @return UtilisateurDao
     */
    public function getUserDao()
	{
		return $this->userDao;
	}
	/**
     * @return null|ControlError
     */
    public function getControl()
    {
        return $this->control;
    }
    /**
     * @return la vue s'il ya error
     */
    public function renderView()
    {
        return $this->control->renderView();
    }

}