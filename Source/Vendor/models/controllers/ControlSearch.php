<?php
namespace Vendor\models\controllers;

use Vendor\models\dao\ModuleDao;
use Vendor\models\dao\UtilisateurDao;
use Vendor\models\linkers\Utilisateur;

class ControlSearch implements Control
{
    private $pathViews;
    private $view;
    private $userDao;
    private $moduleDao;

    private $search;

    private $status;
    private $user_results;
    private $mod_results;

    /**
     * ControlSearch constructor.
     * Définit les valeurs des attributs.
     * @param $pathViews string racine des vues
     * @param $view string vue à afficher
     * @param $userDao UtilisateurDao intéraction avec la base de données
     * @param $moduleDao ModuleDao intéraction avec la base de données
     * @param $dataPost array données POST du formulaire
     */
    public function __construct($pathViews, $view, $userDao, $moduleDao, $dataPost)
    {
        $this->pathViews = $pathViews;
        $this->view = $view;
        $this->userDao = $userDao;
        $this->moduleDao = $moduleDao;

        $this->search = (isset($dataPost['recherche'])) ? htmlspecialchars($dataPost['recherche']) : null;

        $this->status = array();
        $this->user_results = array();
        $this->mod_results = array();
    }

    /*
     * Tente d'effectuer une recherche.
     */
    public function doSearch()
    {
        $msg = 'Résultat(s) pour la recherche : "' . $this->search . '"';
        if (empty($this->search)) {
            $id_search_ok = null;
            $msg = 'Veuillez procéder à une recherche.';
        } else {
            $this->user_results = $this->userDao->findByName($this->search);
            $this->mod_results = $this->moduleDao->findByName($this->search);

            // On récupére l'id de l'enseignant référent
            // afin d'obtenir son nom et prénom
            foreach ($this->mod_results as $module) {
                $ensRef = $module->getEnseignantReferant();
                if (!is_null($ensRef)) {
                    $user = $this->userDao->getUserById($ensRef);
                    $module->setEnsReferantRef($user);
                }
            }

            // On vérifie qu'il y a au moin un résultat.
            $id_search_ok = count($this->user_results) > 0 || count($this->mod_results) > 0;
            if (!$id_search_ok) {
                $msg = 'Aucun résultat pour la recherche : "' . $this->search . '"';
            }
        }
        $this->status['success'] = $id_search_ok;
        $this->status['msg'] = $msg;
    }

    /**
     * Affiche la vue de l'utilisateur.
     * @return mixed donnée composant la vue
     */
    public function renderView()
    {
        return require_once($this->pathViews.$this->view);
    }

    /**
     * @return null|string
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @return array
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getUserResults()
    {
        return $this->user_results;
    }

    /**
     * @return array
     */
    public function getModResults()
    {
        return $this->mod_results;
    }
}