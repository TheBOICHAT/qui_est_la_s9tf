<?php
namespace Vendor\models\controllers;

interface Control
{
    /**
     * Affiche la vue de l'utilisateur.
     * @return mixed donnée composant la vue
     */
    public function renderView();
}