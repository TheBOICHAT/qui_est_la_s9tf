<?php
namespace Vendor\models\controllers;

use Vendor\models\dao\EchecDeConnexionDBException;
use Vendor\models\dao\LoginUtilisateurDejaPresentException;
use Vendor\models\dao\UtilisateurDao;
use Vendor\models\linkers\EnumRole;
use Vendor\models\linkers\Utilisateur;

/** Class ControlUser Gére l'intéraction avec le formulaire des utilisateurs. */
class ControlUser implements Control
{
    private $pathViews;
    private $view;
    private $userDao;
    private $formChecker;

    // Informations liées au formulaire.
    private $last_name;
    private $first_name;
    private $email;
    private $role;
    private $mdp;
    private $id;

    private $errors;
    private $status;
    private $is_modifing;

    /**
     * Définit les valeurs des attributs.
     * @param $pathViews string racine des vues
     * @param $view string vue à afficher
     * @param $userDao UtilisateurDao intéraction avec la base de données
     * @param $formChecker CheckFormUser classe permettant de vérifier les données du formulaire
     * @param $dataPost array données POST du formulaire
     */
    public function __construct($pathViews, $view, $userDao, $formChecker, $dataPost)
    {
        $this->pathViews = $pathViews;
        $this->view = $view;
        $this->userDao = $userDao;
        $this->formChecker = $formChecker;

        $this->last_name = (isset($dataPost['nom'])) ? htmlspecialchars($dataPost['nom']) : null;
        $this->first_name = (isset($dataPost['prenom'])) ? htmlspecialchars($dataPost['prenom']) : null;
        $this->email = (isset($dataPost['email'])) ? htmlspecialchars($dataPost['email']) : null;
        $this->role = (isset($dataPost['role'])) ? $this->defineValue($dataPost['role']) : null;
        $this->mdp = (isset($dataPost['mdp'])) ? htmlspecialchars($dataPost['mdp']) : null;
        $this->id = (isset($dataPost['id'])) ? $this->defineValue($dataPost['id']) : null;

        $this->errors = array();
        $this->status = array();
        $this->is_modifing = false;
    }

    /**
     * Initialise les entiers correctement
     * de façon à détecter les différentes erreurs.
     * @param $to_process int donnée à traiter
     * @return int donnée traitée
     */
    private function defineValue($to_process)
    {
        if (!empty($to_process)) {
            if (is_numeric($to_process)) {
                // Valeur choisi et bien formé
                return (int)$to_process;
            } else {
                // Valeur choisi et mal formé
                return -1;
            }
        }
        // Aucune valeur choisi
        return $to_process;
    }

    /**
     * Remet à zéro les valeurs affichées dans le formulaire.
     */
    public function resetData()
    {
        $this->last_name = '';
        $this->first_name = '';
        $this->email = '';
        $this->role = -1;
        $this->id = -1;
    }

    /**
     * Création de l'utilisateur dans la base de données.
     */
    public function requestCreation()
    {
        $this->errors = $this->formChecker->checkFormErrors($this->last_name, $this->first_name, $this->email, $this->role);
        if (empty($this->errors)) {

            $currentUser = $this->buildCurrentUser();

            // Un étudiant n'a pas besoin de mot de passe
            if ($this->role !== EnumRole::Etudiant) {
                // Le mot de passe par défaut est composé de 4 chiffres.
                $this->mdp = substr(rand(), 0, 4);
                $currentUser->setMotDePass(password_hash($this->mdp,PASSWORD_DEFAULT));
            } else {
                $this->mdp = null;
            }

	        $is_user_created = true;
            $msg = "Utilisateur créée avec succès !";
            // TODO : temporaire pour afficher le mot de passe initial.
	        if($currentUser->getRoleModel() !== EnumRole::Etudiant) {
	            $this->status['msg_mdp'] = "Le mot de passe est : ".$this->mdp;
            } else {
                $this->status['msg_mdp'] = "Aucun mot de passe pour les étudiants.";
            }

            try {
                $this->userDao->add($currentUser);
            } catch (EchecDeConnexionDBException $e) {
                $is_user_created = false;
                $msg = "Erreur lors de la création de l'utilisateur (".$e->getMessage().").";
            } catch (LoginUtilisateurDejaPresentException $e) {
                $is_user_created = false;
                $msg = "Un utilisateur existe déjà pour cette adresse email.";
            }

            if ($is_user_created) {
                /*
                if (!$this->sendEmailDefaultPassword($currentUser)) {
                    $is_user_created = false;
                    $msg = 'Erreur lors de l\' envoi de l\'email contenant le mot de passe initial.';
                }
                */

                $this->resetData();
            }

            $this->status['success'] = $is_user_created;
            $this->status['msg'] = $msg;
        }
    }

    /**
     * Récupération des informations d'un utilisateur déjà existant.
     */
    public function fetchInformation()
    {
        $this->status = $this->formChecker->checkIdErrors($this->id);
        if (empty($this->status)) {
            try {
                $user = $this->userDao->getUserById($this->id);
                if (empty($user)) {
                    $this->status['success'] = false;
                    $this->status['msg'] = "Aucun utilisateur ne correspond à l'identifiant.";
                } else {
                    $this->is_modifing = true;
                    $this->last_name = $user->getNom();
                    $this->first_name = $user->getPrenom();
                    $this->email = $user->getMail();
                    $this->role = $user->getRoleModel();
                }

            } catch (EchecDeConnexionDBException $e) {
                $this->status['success'] = false;
                $this->status['msg'] = "Erreur lors de l'obtention des informations de l'utilisateur (".$e->getMessage().").";
            }
        }
    }

    /**
     * Modification des informations d'un utilisateur déjà existant.
     */
    public function requestModification()
    {
        $this->errors = $this->formChecker->checkFormErrors($this->last_name, $this->first_name, $this->email, $this->role);
        $this->status = $this->formChecker->checkIdErrors($this->id);
        if (empty($this->errors) and empty($this->status)) {

            $currentUser = $this->buildCurrentUser();
            if ($this->role !== EnumRole::Etudiant and !empty($this->mdp)) {
                $currentUser->setMotDePass(password_hash($this->mdp, PASSWORD_DEFAULT));
            }

            $is_user_modified = true;
            $msg = "Informations modifiées avec succès !";
            try {
                $this->userDao->update($currentUser);
            } catch(EchecDeConnexionDBException $e) {
                $is_user_modified = false;
                $msg = "Erreur lors de la modification des informations (".$e->getMessage().").";
            }
            $this->status['success'] = $is_user_modified;
            $this->status['msg'] = $msg;
        }
        $this->is_modifing = true;
    }

    /**
     * Supprime un utilisateur déjà existant.
     */
    public function requestDeletion()
    {
        $this->status = $this->formChecker->checkIdErrors($this->id);
        if (empty($this->errors) and empty($this->status)) {
            $currentUser = $this->buildCurrentUser();
            $is_user_deleted = true;
            $msg = "Utilisateur supprimé avec succès !";
            try {
                $this->userDao->delete($currentUser);
            } catch (EchecDeConnexionDBException $e) {
                $is_user_deleted = false;
                $msg = "Erreur lors de la suppression de l'utilisateur (".$e->getMessage().").";
            }
            $this->status['success'] = $is_user_deleted;
            $this->status['msg'] = $msg;
        }
        $this->resetData();
    }

    public function sendEmailDefaultPassword($currentUser)
    {
        $to = $currentUser->getMail();
        $subject = '[Qui est là ?] Mot de passe par défaut';
        $message =
            'Bonjour,\r\n'.
            'Voici votre mot de passe généré aléatoirement : ' . $this->mdp . '\r\n' .
            'Veuillez le modifier à la prochaine connexion !\r\n' .
            'Ceci est un message automatique.';
        $headers = 'From: auto@qui_est_la.fr\r\n' .
            'X-Mailer: PHP/' . phpversion();
        return mail($to, $subject, $message, $headers);
    }

    /**
     * Créer une instance d'utilisateur avec les informations du formulaire.
     * @return Utilisateur utilisateur crée
     */
    private function buildCurrentUser()
    {
        $currentUser = new Utilisateur(
            $this->last_name,
            $this->first_name,
            $this->role
        );

        $currentUser->setMail($this->email);

        if (!empty($this->id) and $this->id > -1) {
            $currentUser->setId($this->id);
        }
        return $currentUser;
    }

    /**
     * Affiche la vue de l'utilisateur.
     * @return mixed donnée composant la vue
     */
    public function renderView()
    {
        return require_once($this->pathViews.$this->view);
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return int
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isModifing()
    {
        return $this->is_modifing;
    }

    /**
     * @return null|string
     */
    public function getMdp()
    {
        return $this->mdp;
    }
}
