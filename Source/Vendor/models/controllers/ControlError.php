<?php
namespace Vendor\models\controllers;

class ControlError implements Control
{
    private $pathViews;
    private $view;
    private $message;
    private $detail;

    public function __construct($pathViews, $view, $message, $detail='')
    {
        $this->pathViews = $pathViews;
        $this->view = $view;
        $this->message = $message;
        $this->detail = $detail;
    }

    /**
     * Affiche la vue de l'utilisateur.
     * @return mixed donnée composant la vue
     */
    public function renderView()
    {
        return require_once($this->pathViews.$this->view);
    }
}