<?php

namespace Vendor\models\controllers;

class CheckFormConnexion {

    public function checkConxErrors($login, $motDePass)
    {
        $errors = array();
        if (empty($login)) {
            $errors['login'] = "Veuillez renseigner votre Email.";
        }
        if (empty($motDePass)) {
            $errors['mdp'] = "Veuillez renseigner votre mot de passe.";
        }

        return $errors;
    }
}