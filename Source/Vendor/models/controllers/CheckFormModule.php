<?php
namespace Vendor\models\controllers;

class CheckFormModule
{
    /**
     * Vérification des erreurs liées au remplissage du formulaire.
     */
    public function __construct()
    {

    }

    public function checkFormErrors($nom,$ensRef)
    {

        $errors = array();
        if (empty($nom)) {
            $errors['nom'] = "Veuillez renseigner le libellé de module.";
        }
        if ($ensRef==0) {
            $errors['ensRef'] = "Veuillez choisir un enseignant réferent";
        }
        return $errors;

    }

    /**
     * Vérification des erreurs liées à l'identificateur de module à modifier.
     * @param $id int identifiant d'un module déjà existant
     * @return array erreurs liées aà l'identificateur
     */
    public function checkIdErrors($id)
    {
        $status = array();
        if ($id === -1) {
            $status['success'] = false;
            $status['msg'] = "Identifiant module invalide.";
        }
        return $status;
    }
}
