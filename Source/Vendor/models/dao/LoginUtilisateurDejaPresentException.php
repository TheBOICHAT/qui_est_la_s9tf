<?php
namespace Vendor\models\dao;


class LoginUtilisateurDejaPresentException extends \Exception
{
    /**
     *  UtilisateurDejaPresentException
     */
    public function __construct($message,$code=10,$previus=null)
    {
        parent::__construct($message,$code,$previus);
    }

    public function __toString()
    {
        return parent::__toString();
    }
}