<?php
namespace Vendor\models\dao;

use Vendor\models\linkers\Abscence;

class AbscenceDao
{
    private $factory;
    private $table="absence";
    private $fields=array(
        "field1" =>"idAbsence",
        "field2" =>"abs_date",
        "field3" =>"motif",
        "field4" =>"commentaire_enseignant",
        "field5" =>"absence_justifie",
        "field6" =>"abs_idModule",
        "field7" =>"abs_idEtudiantant",
        "field8" =>"abs_idEnseignant"
    );

    /**
     * AbscenceDao constructor.
     * @param $factory
     */
    public function __construct($factory)
    {
        $this->factory = $factory;
    }

    /**
     * Ajoute une absence dans la base de données
     * @param Abscence $abscence
     * @throws EchecDeConnexionDBException
     */
    public function add($abscence){
        $dbAccess=$this->factory->getConnexion();
        try{
            //connexion a la base de données
            $connexion=$dbAccess->dbConnect();
            $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
            //préparation de la requête d'ajout
            $request=$connexion->prepare(
                "INSERT INTO ".
                " ".$this->table."(".
                $this->fields["field1"].", ".
                $this->fields["field2"]." ,".
                $this->fields["field3"].", ".
                $this->fields["field4"].", ".
                $this->fields["field5"].", ".
                $this->fields["field6"].", ".
                $this->fields["field7"].", ".
                $this->fields["field8"].") ".
                "VALUES(".
                ":value1,".
                ":value2,".
                ":value3,".
                ":value4,".
                ":value5,".
                ":value6,".
                ":value7,".
                ":value8)"
            );
            //exécution de la requête avec les valeurs des champs
            $request->execute(array(
                'value1' => utf8_decode(null),
                'value2' => utf8_decode($abscence->getDate()->format("d-m-y")),
                'value3' => utf8_decode($abscence->getMotif()),
                'value4' => utf8_decode($abscence->getCommentaire()),
                'value5' => utf8_decode($abscence->getStatus()),
                'value6' => utf8_decode($abscence->getIdEnseignant()),
                'value7' => utf8_decode($abscence->getIdEtudiant()),
                'value8' => utf8_decode($abscence->getIdModule())
            ));
        }
        catch( EchecDeConnexionDBException $e){
            throw new EchecDeConnexionDBException($e->getMessage());
        }
    }

    /**
     * supprime une absence de la base de données quand l'identifiant de celle-ci est présente dans la base de données
     * @param Abscence $abscence
     * @throws EchecDeConnexionDBException
     */
    public function delete($abscence){
        $dbAccess=$this->factory->getConnexion();
        try{
            $connexion=$dbAccess->dbConnect();
            $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);

            $request=$connexion->prepare("DELETE FROM ".$this->table." WHERE ".$this->fields["field1"]."=:idValue");
            $request->execute(array(
                'idValue' => utf8_decode($abscence->getIdAbscence())
            ));
        }
        catch(EchecDeConnexionDBException $e){
            throw  new EchecDeConnexionDBException($e->getMessage());
        }
    }



}