<?php
namespace Vendor\models\dao;

class ConnexionToServer
{
    /*
     * la configuration de la connexion au serveur de la base de données
     *['db_hostname','db_name','db_user','db_password']
     */
    private $configuration;

    /*
     * nom du fichier(ou chemin relatif)  de configfuratioin à la base de données
     */
    private $configFileName;

    /**
     * ConnexionToServer constructor.
     * @param $configFileName : le fichier contenant les paramètres de connexion qu serveur
     * @throws ParsingConfigFileException
     */
    public function __construct($configFileName)
    {
        $this->configFileName = $configFileName;
        $this->configuration=parse_ini_file($this->configFileName);
        if($this->configuration==false){
            //une erreur est survenue lors du parsing du fichier de configuration passé en parametre
            throw new ParsingConfigFileException("_ERR_: fichier de configuration non trouvé ");
        }
    }


    /**
     * Établit une connexion avec la base de donnéés  avec les paramètres de connexion définis dans l'attribut configuration
     * @throws  EchecDeConnexionDBException en cas d'échec de connexion
     * @return null|PDO : renvoie null en cas d'échec , une connexion avec la base de données sinon
     */
    public function dbConnect(){
        $suffix = '';
        if (gethostname() !== 'm2gl') {
            $suffix = '_local';
        }

        $connexion=null;
        try {
            $connexion = new \PDO('mysql:host=' . $this->configuration['db_hostname'.$suffix] . ';dbname=' . $this->configuration['db_name'.$suffix], $this->configuration['db_user'.$suffix], $this->configuration['db_password'.$suffix]);
            return $connexion;
        }
        catch(\PDOException $e){
            //"_ERR_: Echec de connexion á la base de données "
            throw new EchecDeConnexionDBException($e->getMessage());
        }
    }

    /**
     * Établit une connexion avec la base de donnéés  avec les paramètres passé en paramètre
     * @param $dbHostName: nom de la machine hote de la base de données
     * @param $dbName:  nom de la base de données
     * @param $dbUser: non de l'utilisateur
     * @param $dbPassWord: mot de pass de l'utilisateur passé en paramètre
     * @throws  EchecDeConnexionDBException en cas d'échec de connexion
     * @return PDO : renvoie null en cas d'échec , une connexion avec la base de données sinon
     */
    public function  dbConnectWith($dbHostName,$dbName,$dbUser,$dbPassWord){
        $connexion=null;
        try{
            $connexion=new \PDO('mysql:host='.$dbHostName.'username='.$dbName, $dbUser, $dbPassWord);
            return $connexion;
        }
        catch(PDOException $e){
            throw new EchecDeConnexionDBException($e->getMessage());
        }
    }



}
