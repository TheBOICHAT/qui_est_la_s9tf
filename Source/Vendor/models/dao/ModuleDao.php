<?php
namespace Vendor\models\dao;

use Vendor\models\linkers\Module;

class ModuleDao
{
    private $factory;
    private $table = "Module";
    private $field=array(
        "id" => "idModule",
        "libelle" => "libelle",
        "ens_ref" => "enseignant_referent"
    );

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    public function __construct($factory)
    {
        $this->factory = $factory;
    }

    /**
     * Recherche les modules par leur nom
     * @param $search string donnée recherchée
     * @return array résulat(s) de la recherche
     */
    public function findByName($search) {
        $dbAccess=$this->factory->getConnexion();
        $connexion=$dbAccess->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $request = $connexion->prepare(
            "SELECT * FROM " . $this->table . " " .
            "WHERE ".$this->field['libelle']." LIKE :search"
        );

        $request->execute(array(
            'search' => '%' . utf8_decode($search) . '%'
        ));

        $moduleResult = array();
        while ($result = $request->fetch(\PDO::FETCH_ASSOC)) {
            $module = new Module();
            if (!empty($result[$this->field["ens_ref"]])) {

            }
            $module->setEnseignantReferant($result[$this->field["ens_ref"]]);
            $module->setLibelle(utf8_encode($result[$this->field["libelle"]]));
            $module->setIdModule($result[$this->field["id"]]);
            array_push(
                $moduleResult,
                $module
            );
        }
        return $moduleResult;
    }

    public function update($module) {
        $dbAccess=$this->factory->getConnexion();
        try {
            $connexion=$dbAccess->dbConnect();
            $connexion->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $request = $connexion->prepare(
                "UPDATE ".$this->table." SET "
                .$this->field['libelle']." = :value2,"
                .$this->field['ens_ref']." = :value3 "
                ."WHERE ".$this->field['id']." = :value1 ");
            $request->execute(array(
                'value1' => utf8_decode($module->getIdModule()),
                'value2' => utf8_decode($module->getLibelle()),
                'value3' => utf8_decode($module->getEnseignantReferant())
            ));
            return 0;
        }
        catch (\Exception $e) {
            $request->debugDumpParams();
            echo "module update : echec ".$e->getMessage();
            return -1;
        }
    }

    //tested OK
    public function add($module){
        $dbAccess=$this->factory->getConnexion();
        try{
            $connexion=$dbAccess->dbConnect();
            $connexion->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $request=$connexion->prepare(
                "INSERT INTO ".
                $this->table."(".
                $this->field["id"].", ".
                $this->field["libelle"].", ".
                $this->field["ens_ref"].") ".
                "VALUES (".
                ":value1,".
                ":value2,".
                ":value3 )"
            );

            $request->execute(array(
                'value1' => null,
                'value2' => utf8_decode($module->getLibelle()),
                'value3' => utf8_decode($module->getEnseignantReferant())
            ));

            $last_id = $connexion->lastInsertId();
            return $last_id;
        }
        catch (\Exception $e) {
            $request->debugDumpParams();
            echo "Add Module : Echec de connection a la base de donnée".$e->getMessage();
            return -1;
        }
    }

    //tested OK
    public function delete($module) {
        $dbAccess=$this->factory->getConnexion();
        try{
            $connexion=$dbAccess->dbConnect();
            $connexion->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $request=$connexion->prepare(
                "DELETE FROM ".$this->table." WHERE ".$this->field["id"]." = :value1"
            );
            $request->execute(array('value1' => utf8_decode($module->getIdModule())));
            return 0;

        }
        catch (\Exception $e) {
            $request->debugDumpParams();
            echo "Delete Module : Echec de connection a la base de donnée".$e->getMessage();
            return -1;
        }
    }

    public function getAll() {
        $res = array();
        $dbAccess=$this->factory->getConnexion();
        try {
            $stmt = $dbAccess->dbConnect()->query("SELECT * FROM " . $this->table);
            while ($row = $stmt->fetch()) {
                $module = new Module();
                $module->setEnseignantReferant($row[$this->field["ens_ref"]]);
                $module->setLibelle($row[$this->field["libelle"]]);
                $module->setIdModule($row[$this->field["id"]]);
                $res[] = $module;
            }
            return $res;
        }
        catch (\Exception $e) {
            echo "Get all module : echec ".$e->getMessage();
            return -1;
        }
    }

    public function getById ($id) {
        $module = new Module();
        $dbAccess=$this->factory->getConnexion();
        $module->setIdModule(-1);
        try {
            $stmt = $dbAccess->dbConnect()->query("SELECT * FROM " . $this->table . " WHERE " . $this->field["id"] . "=" . $id);
            $row = $stmt->fetch();
            if (!empty($row)) {

                $module->setEnseignantReferant($row[$this->field["ens_ref"]]);
                $module->setLibelle($row[$this->field["libelle"]]);
                $module->setIdModule($row[$this->field["id"]]);
                return $module;
            }
        }
        catch (\Exception $e) {
            echo "module getById : echec ".$e->getMessage();
            return $module;
        }
        return null;
    }


    /**
     * renvoie true si l'id passé en paramètre est présent dans la table des modules, false sinon
     * @param int $id : l'id dont on teste la présence dans la table
     * @return bool
     */
    public function existId(int $id)
    {
        $dbAccess=$this->factory->getConnexion();
        $stmt = $dbAccess->dbConnect()->query("SELECT ".$this->field["id"]." FROM " . $this->table . " WHERE " . $this->field["id"] . "=" . $id);
        $row=$stmt->fetch();
        return ($row->rowCount>0);
    }




}