<?php

namespace Vendor\models\dao;
use Vendor\models\linkers\EnumRole;
use Vendor\models\linkers\Utilisateur;

/**
 * Class UtilisateurDao
 * @package Vendor\models\dao
 */
class UtilisateurDao
{
    //élément de connexion
    private $factory;
    //table associée dans la base de données
    private $table="Utilisateur";

    //on utilise un RoleDao pour verifier que le role de l'utilisateur existe dans la table des rôles
    private $daoRoleChecker;
    //les champs de la table
    private $fields=array(
        "id" =>"idUtilisateur",
        "prenom" =>"prenom",
        "nom" =>"nom",
        "email" =>"email",
        "role" =>"role",
        "mdp" =>"mdp"
    );


    /**
     * UtilisateurDao constructor.
     * @param $factory
     */
    public function __construct($factory)
    {
        $this->factory = $factory;
    }
    
    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * @param string $table
     */
    public function setTable($table)
    {
        $this->table = $table;
    }

    public function add($utilisateur){
        $dbAccess=$this->factory->getConnexion();
        try{
            $newUser=$this->existEmail($utilisateur->getMail());
            if($newUser==true){
                //l'adresse mail de l'utilisateur est deja présente dans la table des utilisateurs
                throw new LoginUtilisateurDejaPresentException("__ERR_: l'utilisateur que vous tentez d'ajouter existe déja dans la base de données ");
            }
            else{
                //le nouvel utilisateur n'est pas présent dans la table : l'ajout st donc possible
                $dbAccess=$this->factory->getConnexion();
                $connexion=$dbAccess->dbConnect();
                $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
                $request=$connexion->prepare(
                    "INSERT INTO ".
                    " ".$this->table."(".
                    $this->fields["id"].", ".
                    $this->fields["prenom"]." ,".
                    $this->fields["nom"].", ".
                    $this->fields["email"].", ".
                    $this->fields["role"].", ".
                    $this->fields["mdp"]." )".
                    "VALUES(".
                    ":id,".
                    ":prenom,".
                    ":nom,".
                    ":email,".
                    ":role,".
                    ":mdp)"
                );
                $request->execute(array(
                    'id' => null,
                    'nom' => utf8_decode($utilisateur->getNom()),
                    'prenom' => utf8_decode($utilisateur->getPrenom()),
                    'email' => utf8_decode($utilisateur->getMail()),
                    'role' => utf8_decode($utilisateur->getRoleDB()),
                    'mdp' => utf8_decode($utilisateur->getMotDePass())

                ));
                return $connexion->lastInsertId();
            }
        }
        catch( EchecDeConnexionDBException $e){
            //echec de connexion a la base de données
            throw new EchecDeConnexionDBException($e->getMessage());
        }
    }

    /**
     * Ajoute un utilisateur passé en parametre dans la base de données
     * @param Utilisateur $utilisateur
     * @throws EchecDeConnexionDBException
     */
    public function delete($utilisateur){
        $dbAccess=$this->factory->getConnexion();
        try{
            $connexion=$dbAccess->dbConnect();
            $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);

            $request=$connexion->prepare("DELETE FROM ".$this->table." WHERE ".$this->fields["id"]."=:id");
            $request->execute(array(
               'id' => utf8_decode($utilisateur->getId())
            ));
        }
        catch(\Exception $e){
            throw  new EchecDeConnexionDBException($e->getMessage());
        }
    }

    /*
     */
    public function update($utilisateur)
    {
        $dbAccess=$this->factory->getConnexion();
        try{
            $connexion=$dbAccess->dbConnect();
            $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
            $request = "UPDATE " . $this->table." SET " .
                $this->fields['prenom']."=:prenom, " .
                $this->fields['nom']."=:nom, " .
                $this->fields['email']."=:email, " .
                $this->fields['role']."=:role ";

            if (!empty($utilisateur->getMotDePass())) {
                $request .= ", " . $this->fields['mdp']."=:mdp ";
            }

            $request .= "WHERE ".$this->fields['id']."=:id";

            $query=$connexion->prepare($request);

            $queryValues = array(
                'id' => utf8_decode($utilisateur->getId()),
                'nom' => utf8_decode($utilisateur->getNom()),
                'prenom' => utf8_decode($utilisateur->getPrenom()),
                'email' => utf8_decode($utilisateur->getMail()),
                'role' => utf8_decode($utilisateur->getRoleDB())
            );

            if (!empty($utilisateur->getMotDePass())) {
                $queryValues['mdp'] = utf8_decode($utilisateur->getMotDePass());
            }

            $query->execute($queryValues);
        }
        catch(EchecDeConnexionDBException $e){
            throw new EchecDeConnexionDBException($e->getMessage());
        }
    }

    /**
     * @param int $userId
     * @return Utilisateur
     * @throws EchecDeConnexionDBException
     */
    public function getUserById($userId)
    {
        $connexion=$this->factory->getConnexion()->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
        $query=$connexion->prepare("SELECT * FROM ".$this->table." WHERE ".$this->fields["id"]."=".$userId);
        $query->execute();
        $rowResult=$query->fetch(\PDO::FETCH_ASSOC);
        if($query->rowCount()>0){
            //il y'a un utilisateur correspendant a cet id
            $user=new Utilisateur(
                $rowResult[$this->fields["nom"]],
                $rowResult[$this->fields["prenom"]],
                $rowResult[$this->fields["role"]]
            );
            $user->setId($rowResult[$this->fields["id"]]);
            $user->setMail($rowResult[$this->fields["email"]]);
            $user->setMotDePass($rowResult[$this->fields["mdp"]]);
            return $user;
        }
        return null;
    }

    /**
     * renvoie les informations d'un utilisateur si celui-ci a un compte valide
     * @param Utilisateur $user
     * @return bool
     * @throws EchecDeConnexionDBException
     */
    public function authentifyUser($mail, $password)
    {
        $connexion=$this->factory->getConnexion()->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
        $query= $connexion->prepare("SELECT DISTINCT * FROM ".$this->table.
                                    " WHERE ".$this->fields["mdp"]." = :password AND ( ".$this->fields["email"]." = :login )");
        $query->execute(array(
                        'password' =>utf8_decode($password),
                        'login' =>utf8_decode($mail))
        );
        $rowResult=$query->fetch(\PDO::FETCH_ASSOC);
        if($query->rowCount()>0){
            //il y'a un utilisateur correspendant a cet id
            $user=new Utilisateur($rowResult[$this->fields["nom"]],
                $rowResult[$this->fields["prenom"]],
                EnumRole::toModel($rowResult[$this->fields["role"]])
            );
            $user->setId($rowResult[$this->fields["id"]]);
            $user->setMail($rowResult[$this->fields["email"]]);
            $user->setMotDePass($rowResult[$this->fields["mdp"]]);
            return $user;
        }
        //l'utilisateur n'a pas de compte 
        return null;
    }

    /**
     * @param $mail
     * @return bool
     * retourne true si l'email passer em parametre est deja dans la bdd
     *
     *
     */
    public function existEmail($mail)
    {
        $connexion=$this->factory->getConnexion()->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
        $query= $connexion->prepare("SELECT ".$this->fields["email"]." FROM ".$this->table." WHERE ".$this->fields["email"]."=:login ");
        $query->execute(array('login' =>utf8_decode($mail)));
        $rowResult=$query->fetch(\PDO::FETCH_ASSOC);
        return ($query->rowCount()!=0);
    }


    /**
     * renvoie un utilisateur dont le mail est est présent dans la base de donnnées , null sinon
     * @param string $mail
     * @return Utilisateur
     * @throws EchecDeConnexionDBException
     */
    public function findByEmail($mail)
    {
        $connexion=$this->factory->getConnexion()->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
        $query= $connexion->prepare("SELECT * FROM ".$this->table." WHERE ".$this->fields["email"]."=:login ");
        $query->execute(array('login' =>utf8_decode($mail)));
        $rowResult=$query->fetch(\PDO::FETCH_ASSOC);
        if($query->rowCount()>0){
            //il existe un utilisateur qui a ce mail

            $user=new Utilisateur($rowResult[$this->fields["nom"]],
                $rowResult[$this->fields["prenom"]],
                $rowResult[$this->fields["role"]]
            );
            $user->setId($rowResult[$this->fields["id"]]);
            $user->setMail($rowResult[$this->fields["email"]]);
            $user->setMotDePass($rowResult[$this->fields["mdp"]]);
            return $user;
        }
        //il n'y a pas d'utilisateur ayant ce mail
        return  null;
    }


    public function getAllEnseignant(){
        $connexion=$this->factory->getConnexion()->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
        $query= $connexion->prepare("SELECT * FROM ".$this->table." WHERE ".$this->fields["field4"]."=:login ");
    }

    public function getAllEnseignantRef(){

        $connexion=$this->factory->getConnexion()->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);

        $sth = $connexion->prepare("SELECT * FROM ".$this->table." WHERE ".$this->fields["role"]."='enseignant'");

        $sth->execute();
        return $row = $sth->fetchAll();


    }


    public function getAllEtudiant(){


    }

    public function getAllPersonnel(){

    }

    /**
     * Renvoie si le role passé en parametre existe dans table des utilisateurs
     * @param string $role
     * @return bool
     * @throws EchecDeConnexionDBException
     */
    public function existRole($role)
    {
        $connexion=$this->factory->getConnexion()->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
        $query= $connexion->prepare("SELECT * FROM ".$this->table." WHERE ".$this->fields["role"]."=:role ");
        $query->execute(array('role' =>utf8_decode($role)));
        $rowResult=$query->fetch(\PDO::FETCH_ASSOC);
        if($query->rowCount()>0) {
            return true;
        }
        return false;
    }

    /**
     * Recherche les utilisateurs par leur nom
     * @param $search string donnée recherchée
     * @return array résulat(s) de la recherche
     */
    public function findByName($search)
    {
        $connexion = $this->factory->getConnexion()->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $query = $connexion->prepare(
            "SELECT * FROM " . $this->table . " " .
            "WHERE " . $this->fields["nom"] . " LIKE :search "
        );

        $params = array('search' => '%' . utf8_decode($search) . '%');

        $query->execute($params);

        $usersResult = array();
        while ($result = $query->fetch(\PDO::FETCH_ASSOC)) {
            $newUser = new Utilisateur(
                $result[$this->fields['nom']],
                $result[$this->fields['prenom']],
                $result[$this->fields['role']]
            );
            $newUser->setId($result[$this->fields['id']]);

            array_push(
                $usersResult,
                $newUser
            );
        }
        return $usersResult;
    }
}