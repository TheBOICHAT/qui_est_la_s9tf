<?php

namespace Vendor\models\dao;


class RoleUtilisateurInexistantException extends \Exception
{
    /**
     *  RoleUtilisateurInexistantException
     */
    public function __construct($message,$code=0,$previus=null)
    {
        parent::__construct($message,$code,$previus);
    }

    public function __toString()
    {
        return parent::__toString();
    }
}