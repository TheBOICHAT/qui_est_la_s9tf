<?php

namespace Vendor\models\dao;


class RoleDao
{
    private $factory;
    private $table="roles";
    private $fields=[
        "field1" => "idRole",
        "field2" => "libele"
    ];

    /**
     * RoleDao constructor.
     * @param $factory
     */
    public function __construct($factory)
    {
        $this->factory = $factory;
    }


    /**
     * renvoie true si l'id passé en paramètre est présent dans la table des modules, false sinon
     * @param int $id : l'id dont on teste la présence dans la table
     * @return bool
     */
    public function existId(int $id)
    {
        $connexion=$this->factory->getConnexion()->dbConnect();
        $connexion->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);

        $query=$connexion->prepare(" SELECT idRole FROM roles WHERE idRole=:value");
        $query->execute(array("value" => utf8_decode($id)));
        $row=$query->fetch(\PDO::FETCH_ASSOC);
        return ($query->rowCount()>0);
    }



}