<?php
namespace Vendor\models\dao;

class EchecDeConnexionDBException extends \Exception
{
    /**
     * EchecDeConnexionDBException constructor.
     */
    public function __construct($message,$code=4,$previus=null)
    {
        parent::__construct($message,$code,$previus);
    }

    public function __toString()
    {
        return parent::__toString();
    }
}