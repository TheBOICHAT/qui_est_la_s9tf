<?php
/**
 * Created by PhpStorm.
 * User: chromev
 * Date: 20/10/18
 * Time: 14:55
 */

namespace Vendor\models\dao;

use PhpParser\Node\Expr\Array_;
use Vendor\models\dao\ConnexionToServer;
use Vendor\models\dao\DaoFactory;
use Vendor\models\dao\ModuleDao;
use Vendor\models\dao\UtilisateurDao;
use Vendor\models\linkers\EnumRole;
use Vendor\models\linkers\Module;

require_once("Source/Vendor/models/dao/ModuleDao.php");
require_once("Source/Vendor/models/dao/UtilisateurDao.php");
require_once("Source/Vendor/models/dao/DaoFactory.php");

class UtilisateurHasModuleDao
{
    private $factory;
    public $table = "Utilisateur_has_module";
    private $field=array(
        "idUtil" => "uhm_idUtilisateur",
        "idMod" => "uhm_idModule"
    );
    public $UserDao;
    public $ModuleDao;

    public function setTable($table)
    {
        $this->table = $table;
    }

    public function __construct($factory)
    {
        $this->factory = $factory;
        $this->ModuleDao = new \Vendor\models\dao\ModuleDao($this->factory);
        $this->UserDao = new \Vendor\models\dao\UtilisateurDao($this->factory);

    }

    /**
     * Link the Module @param(idModule) to the Utilisateur @param(idUtilisateur)
     *
     */
    public function link($idModule, $idUtilisateur) {

        $dbAccess=$this->factory->getConnexion();
        try{
            $connexion=$dbAccess->dbConnect();
            $connexion->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $request=$connexion->prepare(
                "INSERT INTO ".
                $this->table."(".
                $this->field["idUtil"].", ".
                $this->field["idMod"].") ".
                "VALUES (".
                ":value1,".
                ":value2 )"
            );
            $request->execute(array(
                'value1' => utf8_decode($idUtilisateur),
                'value2' => utf8_decode($idModule)
            ));
            return 0;
        }
        catch (\Exception $e) {
            $request->debugDumpParams();
            echo "Add Utilisateur_has_module : Echec de connection a la base de donnée".$e->getMessage();
            return -1;
        }
    }

    public function unlink($idModule, $idUtilisateur) {

        $dbAccess=$this->factory->getConnexion();
        try{
            $connexion=$dbAccess->dbConnect();
            $connexion->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $request=$connexion->prepare(
                "DELETE FROM ".$this->table." WHERE ".$this->field["idUtil"]." = :value1"." AND ".$this->field["idMod"]." = :value2"
            );
            $request->execute(array('value1' => utf8_decode($idUtilisateur),
                                    'value2' => utf8_decode($idModule)));
            return 0;

        }
        catch (\Exception $e) {
            $request->debugDumpParams();
            echo "Delete Module : Echec de connection a la base de donnée".$e->getMessage();
            return -1;
        }
    }

    /**
     * @param $idModule
     * return a list of Utilisateur witch are in the module designated by $idModule
     */
    public function getAllUtilisateur($idModule) {
        $dbAccess=$this->factory->getConnexion();
        $res = array();
        try{
            $stmt=$dbAccess->dbConnect()->query("SELECT * FROM ".$this->table." WHERE ".$this->field["idMod"]." = ".$idModule);
            while($row = $stmt->fetch()){
                $res[] = $this->UserDao->getUserById($row[$this->field["idUtil"]]);
            }
            return $res;
        }
        catch (\Exception $e) {
            echo "getAllUtilisateur Utilisateur_has_module : Echec".$e->getMessage();
            return null;
        }
    }

    /**
     * @param $idUtilisateur
     * @return array|null
     * same as detAllUtilisateur but with modules
     */
    public function getAllModule($idUtilisateur) {
        $dbAccess=$this->factory->getConnexion();
        $res = array();
        try{
            $stmt=$dbAccess->dbConnect()->query("SELECT * FROM ".$this->table." WHERE ".$this->field["idUtil"]." = ".$idUtilisateur);
            while($row = $stmt->fetch()){
                $res[] = $this->ModuleDao->getById($row[$this->field["idMod"]]);
            }
            return $res;
        }
        catch (\Exception $e) {
            echo "getAllModule Utilisateur_has_module : Echec".$e->getMessage();
            return null;
        }
    }


}