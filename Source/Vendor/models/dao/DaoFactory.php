<?php
namespace Vendor\models\dao;

class DaoFactory
{
    private $connexion;

    /**
     * DaoFactory constructor.
     */
    public function __construct($connexion)
    {
        $this->connexion=$connexion;

    }

    /**
     * change les paramètres de connexion á la base de données
     * @param string $dbHostName
     * @param string $dbName
     * @param string $dbUser
     * @param string $dbPassWord
     */
    public function setServerConfig($dbHostName,$dbName,$dbUser,$dbPassWord){
        $this->connexion->setConfiguration($dbHostName,$dbName,$dbUser,$dbPassWord);
    }

    /**
     * @return UtilisateurDao
     */
    public function getInstanceOfUtilisateurDao(){
        return new UtilisateurDao($this);
    }
    
    public function  getInstanceOfModuleDao(){
        return new ModuleDao($this);
    }

    public function getInstanceOfAbscenceDao(){
        return new AbscenceDao($this);
    }

    public function checkMolueId(){

    }

    public function checkUserId(){

    }

    public function checkAbsenceId(){

    }


    /**
     * @return DbConnexion
     */
    public function getConnexion()
    {
        return $this->connexion;
    }

    /**
     * @param DbConnexion $connexion
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;
    }


    
    
}