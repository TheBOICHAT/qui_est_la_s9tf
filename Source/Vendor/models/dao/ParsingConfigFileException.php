<?php

namespace Vendor\models\dao;


class ParsingConfigFileException extends \Exception
{
    /**
     * ParsingConfigFileException constructor.
     */
    public function __construct($message,$code=3,$previus=null)
    {
        parent::__construct($message,$code,$previus);
    }

    public function __toString()
    {
        return parent::__toString();
    }
}