<?php
$PAGE_TITLE = 'Connexion';
require_once($this->pathViews."shared/en_tete.php");
?>

<?php require_once($this->pathViews."shared/debut_menu.php");?>
<?php require_once($this->pathViews."shared/fin_menu.php"); ?>

<main role="main" class="container">

    <div class="row">
        <div class="col-sm"></div>
        <form action="connexion.php" method="POST" class="col-sm">
            <p class="text-muted">Veuillez vous authentifier.</p>
            <div class="form-label-group pb-2">
                <label for="idEmail">Adresse email</label>
                <input  id="idEmail" class="form-control" name="idEmail" type="email" placeholder="Adresse email" autofocus>
                <?php if (isset($this->errors['login'])) {echo '<p class="text-danger mb-0"><small id="error_login">'.$this->errors['login'].'</small></p>';} ?>
                <?php if (isset($this->errors['verifyMail'])) {echo '<p class="text-danger mb-0"><small id="error_mail">'.$this->errors['verifyMail'].'</small></p>';} ?>

            </div>

            <div class="form-label-group pb-2">
                <label for="idMotDePasse">Mot de passe</label>
                <input type="password" id="idMotDePasse" class="form-control" name="mdp" placeholder="Mot de passe">
                <?php if (isset($this->errors['mdp'])) {echo '<p class="text-danger mb-0"><small id="error_mdp">'.$this->errors['mdp'].'</small></p>';} ?>
                <?php if (isset($this->errors['verifyMDP'])) {echo '<p class="text-danger mb-0"><small id="error_mdpEr">'.$this->errors['verifyMDP'].'</small></p>';} ?>
                <?php if (isset($this->errors['connecte'])) {echo '<p class="text-success mb-0"><small id="error_cnx">'.$this->errors['connecte'].'</small></p>';} ?>
            </div>

            <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Connexion</button>
        </form>

        <div class="col-sm"></div>

    </div>
</main>

<?php require_once($this->pathViews."shared/pied_page.php");?>
