<?php
$PAGE_TITLE = 'Erreur';
include($this->pathViews."shared/en_tete.php");
include($this->pathViews."shared/debut_menu.php");
include($this->pathViews."shared/fin_menu.php");

echo
    '<main role="main" class="container">' .
        '<div class="alert alert-danger" role="alert">' .
            '<h4 class="alert-heading">Erreur</h4>' .
            '<p>'.$this->message.'</p>' .
            '<hr>' .
            '<p class="mb-0">'.$this->detail.'</p>'.
        '</div>'.
    '</main>';

include($this->pathViews."shared/pied_page.php");
?>
