<?php
use \Vendor\models\linkers\EnumRole;

$PAGE_TITLE = 'Recherche';
require_once($this->pathViews."shared/en_tete.php");
require_once($this->pathViews."shared/debut_menu.php");

echo
    '<div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="module.php">Module<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="utilisateur.php">Utilisateur</a>
            </li>
        </ul>
        <form action="recherche.php" method="post" class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Rechercher" name="recherche" value="'.$this->search.'" aria-label="Rechercher" autofocus>
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
        </form>
    </div>';

require_once($this->pathViews."shared/fin_menu.php");

$template_content =
    '<main role="main" class="container">
        <div class="row">
            <div class="col-sm"></div>
            <div class="col6d-sm">';

if (!empty($this->status)) {
    if (!is_null($this->status['success'])) {
        $color = $this->status['success'] ? 'text-success' : 'text-danger';
        $template_content .= '<p class="'.$color.'"><small id="status_msg">'.$this->status['msg'].'</small></p>';
    } else {
        $template_content .=
            '<div class="alert alert-info" role="alert">
                <h4 class="alert-heading" id="msg_empty_search">Veuillez procéder à une recherche !</h4>
                <p>La recherche porte sur :</p>
                <ul>
                    <li>le nom d\'un utilisateur de type,</li>
                        <ul>
                            <li>enseignant,</li>
                            <li>personnel administratif.</li>
                        </ul>
                    <li>le nom d\'un module.</li>
                </ul>
            </div>';
    }
}

if (!empty($this->user_results)) {
    $template_content .=
        '<form action="utilisateur.php" method="post">
            <table class="table table-striped table-bordered table-hover">
                <thead class="thead-light">
                <tr>
                    <th scope="col">Prénom</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Rôle</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>';

    foreach ($this->user_results as $ens) {
        $template_content .=
            '<tr>' .
            '<td scope="row">' . ucfirst($ens->getPrenom()) . '</td>' .
            '<td>' . strtoupper($ens->getNom()) . '</td>' .
            '<td>' . EnumRole::toString($ens->getRoleModel()) . '</td>' .
            '<td><input type="hidden" name="id" value="'.$ens->getId().'"><input type="submit" name="edit" value="Éditer"></td>' .
            '</tr>';
    }

    $template_content .= '</tbody></table></form>';
}

if (!empty($this->mod_results)) {
    $template_content .=
        '<form action="module.php" method="post">
            <table class="table table-striped table-bordered table-hover">
                <thead class="thead-light">
                <tr>
                    <th scope="col">Module</th>
                    <th scope="col">Référent</th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>';

    foreach ($this->mod_results as $mod) {
        $template_content .=
            '<tr>' .
            '<td scope="row">' . $mod->getLibelle() . '</td>';

        if (is_null($mod->getEnsReferantRef())) {
            $template_content .= '<td>Aucun</td>';
        } else {
            $template_content .= '<td>' . ucfirst($mod->getEnsReferantRef()->getPrenom()) . ' ' . strtoupper($mod->getEnsReferantRef()->getNom()) . '</td>';
        }

        $template_content .= '<td><input type="hidden" name="id" value="'.$mod->getIdModule().'"><input type="submit" name="edit" value="Éditer"></td></tr>';
    }

    $template_content .= '</tbody></table></form>';
}

$template_content .= '</div><div class="col-sm"></div></div></main>';

echo $template_content;

/**
 * Permet de rendre les lignes des tables clickable et
 * de modifier les modules ou utilisateurs liés à une ligne
 */
$javascript_source =
    "$(function(){
        $('.table tr').each(function(){
            $(this).css('cursor','pointer').click(
                function(){
                    $(this).parents('form:first').submit();
                }
            );
        });
    });";

require_once($this->pathViews."shared/pied_page.php");

?>
