<?php
$PAGE_TITLE = 'Module';
include($this->pathViews."shared/en_tete.php");
?>

<?php include($this->pathViews."shared/debut_menu.php");?>
<div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link rounded-0 active" href="module.php">Module<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="utilisateur.php">Utilisateur</a>
        </li>
    </ul>
    <form action="recherche.php" method="post" class="form-inline mt-2 mt-md-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Rechercher" name="recherche" aria-label="Rechercher">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
    </form>
</div>
<?php include($this->pathViews."shared/fin_menu.php");?>

<main role="main" class="container">
    <div class="row">
        <div class="col-sm"></div>
        <div class="col-sm">
            <form action="module.php" method="post" class="form">
                <?php
                if (!empty($this->status)) {
                    $color = $this->status['success'] ? 'text-success' : 'text-danger';
                    echo '<p class="'.$color.' mb-0"><small id="status_msg">'.$this->status['msg'].'</small></p>';
                }
                ?>
                <div class="form-label-group pb-2">
                    <label for="nomModule">Nom du module</label>
                    <input type="text" id="nom" class="form-control" placeholder="Nom du module" name="nom" <?php if (isset($this->nom)) {echo 'value="'.$this->nom.'"';}?> autofocus>
                    <?php
                    if (isset($this->errors['nom'])) {
                         echo '<p class="text-danger mb-0"><small id="error_nom" name="error_nom">'.$this->errors['nom'].'</small></p>';
                    }
                    ?>
                </div>

                <div class="form-label-group pb-2">
                    <label for="ensRef">Enseignant Référent</label>
                    <select name="ensRef" id="ensRef" class="custom-select">
                        <?php
                        if ($this->is_modifing) {
                            echo '<option selected value="'.$this->ensRef.'">'.$this->nomRef.'</option>';
                        } else {
                            echo '<option selected value="0">Aucun</option>';
                        }
                        //echo $this->ens_refs;
                        $length = count($this->ens_refss);
                        $ens_refs=$this->ens_refss;
                        for ($i = 0; $i < $length; ++$i) {
                        if ($ens_refs[$i]['idUtilisateur'] != $this->ensRef) {
                            echo '<option value="' . $ens_refs[$i]['idUtilisateur'] . '">' . $ens_refs[$i]['nom'] . ' ' . $ens_refs[$i]['prenom'] . '</option>';
                        }
                        }
                        ?>
                    </select>
                    <?php if (isset($this->errors['ensRef'])) {echo '<p class="text-danger mb-0"><small id="error_ensref">'.$this->errors['ensRef'].'</small></p>';} ?>

                </div>
                <?php
                if (!empty($this->id)) {
                    echo '<input type="hidden" name="id" value="'.$this->id.'">';
                }


                if ($this->is_modifing) {
                    echo
                        '<div class="container">'.
                        '<div class="form-label-group row">' .
                        '<button name="annuler" type="submit" class="btn btn-outline-secondary col-sm">Annuler</button>' .
                        '<button name="supprimer" class="btn btn-danger col-sm mx-1">Supprimer</button>' .
                        '<button name="modifier" class="btn btn-warning col-sm">Modifier</button>' .
                        '</div>' .
                        '</div>';

                } else {
                    echo '<button id="creer" name="creer" type="submit" class="btn btn-primary">Créer</button>';
                }
                ?>
            </form>

        </div>
        <div class="col-sm">

        </div>
    </div>
    <br><br>

</main>

<?php include($this->pathViews."shared/pied_page.php");?>
