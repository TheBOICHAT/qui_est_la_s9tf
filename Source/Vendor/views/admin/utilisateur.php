<?php
use Vendor\models\linkers\EnumRole;

$PAGE_TITLE = 'Utilisateur';
require_once($this->pathViews."shared/en_tete.php");
require_once($this->pathViews."shared/debut_menu.php");
echo
    '<div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="module.php">Module<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link rounded-0 active" href="utilisateur.php">Utilisateur</a>
            </li>
        </ul>
        <form action="recherche.php" method="post" class="form-inline mt-2 mt-md-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Rechercher" name="recherche" aria-label="Rechercher">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Rechercher</button>
        </form>
    </div>';
require_once($this->pathViews."shared/fin_menu.php");

$template_content =
    '<main role="main" class="container">
        <div class="row">
            <div class="col-sm"></div>
            <div class="col-sm">
                <form action="utilisateur.php" method="post" class="form">';

if (!empty($this->status)) {
    $color = $this->status['success'] ? 'text-success' : 'text-danger';
    $template_content .= '<p class="'.$color.' mb-0"><small id="status_msg">'.$this->status['msg'].'</small></p>';
    if (array_key_exists('msg_mdp', $this->status)) {
        $template_content .= '<p class="text-info mb-0"><small>'.$this->status['msg_mdp'].'</small></p>';
    }
}

$template_content .=
    '<div class="form-label-group pb-2">
        <label for="nom">Nom</label>
        <input type="text" id="nom" class="form-control" placeholder="Nom" name="nom" ';
$template_content .= (isset($this->last_name)) ? 'value="'.$this->last_name.'"' : '';
$template_content .= ((isset($this->errors['nom']) or count($this->errors) === 0) ? ' autofocus' : '') . '>';
$template_content .= (isset($this->errors['nom'])) ? '<p class="text-danger mb-0"><small id="error_nom">'.$this->errors['nom'].'</small></p>' : '';
$template_content .= '</div>';

$template_content .=
    '<div class="form-label-group pb-2">
        <label for="prenom">Prénom</label>
        <input type="text" id="prenom" class="form-control" placeholder="Prénom" name="prenom" ';
$template_content .= (isset($this->first_name)) ? 'value="'.$this->first_name.'"' : '';
$template_content .= ((isset($this->errors['prenom'])) ? ' autofocus' : '') . '>';
$template_content .= (isset($this->errors['prenom'])) ? '<p class="text-danger mb-0"><small id="error_prenom">'.$this->errors['prenom'].'</small></p>' : '';
$template_content .= '</div>';

$template_content .=
    '<div class="form-label-group pb-2">
        <label for="inputEmail">Adresse e-mail</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Adresse e-mail" name="email" ';
$template_content .= (isset($this->email)) ? 'value="'.$this->email.'"' : '';
$template_content .= ((isset($this->errors['email'])) ? ' autofocus' : '') . '>';
$template_content .= (isset($this->errors['email'])) ? '<p class="text-danger mb-0"><small id="error_email">'.$this->errors['email'].'</small></p>' : '';
$template_content .= '</div>';

if ($this->is_modifing and $this->role !== EnumRole::Etudiant) {
    $template_content .=
        '<div class="form-label-group pb-2">
            <label for="inputPassword">Nouveau mot de passe</label>
            <input type="text" id="inputPassword" class="form-control" placeholder="Nouveau mot de passe" name="mdp">
        </div>';
}

$template_content .= '<div class="form-label-group pb-2"><label for="ensRef">Rôle</label>';

$roles = array (
    array('libelle' => 'Administrateur', 'value' => EnumRole::Administrateur),
    array('libelle' => 'Enseignant', 'value' => EnumRole::Enseignant),
    array('libelle' => 'Personnel administratif', 'value' => EnumRole::Personnel),
    array('libelle' => 'Étudiant', 'value' => EnumRole::Etudiant),
);

$length = count($roles);
for ($i = 0; $i < $length; ++$i) {
    $checked = ($this->role === $roles[$i]['value']) ? 'checked' : '';
    $template_content .=
        '<div class="form-check">
            <input class="form-check-input" type="radio" name="role" id="rad'.$i.'" value="'.$roles[$i]['value'].'" '.$checked.'>
            <label class="form-check-label" for="rad'.$i.'">'.$roles[$i]['libelle'].'</label>
        </div>';
}

if (isset($this->errors['role'])) {
    $template_content .= '<p class="text-danger mb-0"><small id="error_role">'.$this->errors['role'].'</small></p>';
}

$template_content .= '</div>';

if (!empty($this->id)) {
    $template_content .= '<input type="hidden" name="id" value="'.$this->id.'">';
}

if ($this->is_modifing) {
    $template_content .=
        '<div class="container">
            <div class="form-label-group row">
                <button type="submit" name="annuler" class="btn btn-outline-secondary col-sm">Annuler</button>
                <button type="submit" name="supprimer" class="btn btn-danger col-sm mx-1">Supprimer</button>
                <button type="submit" name="modifier" class="btn btn-warning col-sm">Modifier</button>
            </div>
        </div>';
} else {
    $template_content .= '<button type="submit" name="creer" class="btn btn-primary">Créer</button>';
}

$template_content .= '</form></div><div class="col-sm"></div></div></main>';

echo $template_content;

require_once($this->pathViews."shared/pied_page.php");
?>
