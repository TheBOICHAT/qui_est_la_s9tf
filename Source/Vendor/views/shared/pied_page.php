<?php
$template_content =
        '<script src="'.$this->pathViews.'static/js/jquery-3.3.1.min.js"></script>' .
        '<script src="'.$this->pathViews.'static/js/bootstrap.bundle.min.js"></script>';
        if (isset($javascript_source)) {
            $template_content .= '<script type="text/javascript">'.$javascript_source.'</script>';
        }
$template_content .= '</body></html>';
echo $template_content;
?>