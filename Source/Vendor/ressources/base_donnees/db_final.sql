-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  Dim 07 oct. 2018 à 16:27
-- Version du serveur :  10.1.36-MariaDB
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `test_php`
--

-- --------------------------------------------------------

--
-- Structure de la table `Absence`
--

CREATE TABLE `Absence` (
  `idAbsence` int(11) NOT NULL,
  `abs_date` date NOT NULL,
  `motif` varchar(255) DEFAULT NULL,
  `commentaire_enseignant` varchar(255) DEFAULT NULL,
  `absence_justifie` tinyint(1) DEFAULT NULL,
  `abs_idModule` int(11) NOT NULL,
  `abs_idEtudiant` int(11) NOT NULL,
  `abs_idEnseignant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Module`
--

CREATE TABLE `Module` (
  `idModule` int(11) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  `enseignant_referent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur`
--

CREATE TABLE `Utilisateur` (
  `idUtilisateur` int(11) NOT NULL,
  `prenom` varchar(20) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `email` varchar(25) NOT NULL,
  `role` enum('etudiant','enseignant','admin','pers_admin') NOT NULL,
  `mdp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `Utilisateur`
--

INSERT INTO `Utilisateur` (`idUtilisateur`, `prenom`, `nom`, `email`, `role`, `mdp`) VALUES
(1, 'thomas', 'boideschats', 'lol@gmail.com', 'etudiant', 'thomas'),
(3, 'fewi', 'kwizy', 'kwizyfery@bordeaux.com', 'etudiant', 'kwizyferi'),
(4, 'paul', 'minette', 'paul@miaou.com', 'enseignant', 'zar rock'),
(5, 'yassine', 'lounani', 'yass@gmail.com', 'admin', 'qqchose');

-- --------------------------------------------------------

--
-- Structure de la table `Utilisateur_has_module`
--

CREATE TABLE `Utilisateur_has_module` (
  `uhm_idUtilisateur` int(11) NOT NULL,
  `uhm_idModule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `Absence`
--
ALTER TABLE `Absence`
  ADD PRIMARY KEY (`idAbsence`),
  ADD KEY `FK_Absence_idModule` (`abs_idModule`),
  ADD KEY `FK_Absence_idEtudiant` (`abs_idEtudiant`),
  ADD KEY `FK_Absence_idEnseignant` (`abs_idEnseignant`);

--
-- Index pour la table `Module`
--
ALTER TABLE `Module`
  ADD PRIMARY KEY (`idModule`),
  ADD KEY `Utilisateur(idUtilisateur)` (`enseignant_referent`);

--
-- Index pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  ADD PRIMARY KEY (`idUtilisateur`);

--
-- Index pour la table `Utilisateur_has_module`
--
ALTER TABLE `Utilisateur_has_module`
  ADD PRIMARY KEY (`uhm_idUtilisateur`,`uhm_idModule`),
  ADD KEY `UHM_FK_idModule` (`uhm_idModule`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `Absence`
--
ALTER TABLE `Absence`
  MODIFY `idAbsence` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `Module`
--
ALTER TABLE `Module`
  MODIFY `idModule` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `Utilisateur`
--
ALTER TABLE `Utilisateur`
  MODIFY `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `Absence`
--
ALTER TABLE `Absence`
  ADD CONSTRAINT `FK_Absence_idEnseignant` FOREIGN KEY (`abs_idEnseignant`) REFERENCES `Utilisateur` (`idUtilisateur`),
  ADD CONSTRAINT `FK_Absence_idEtudiant` FOREIGN KEY (`abs_idEtudiant`) REFERENCES `Utilisateur` (`idUtilisateur`),
  ADD CONSTRAINT `FK_Absence_idModule` FOREIGN KEY (`abs_idModule`) REFERENCES `Module` (`idModule`);

--
-- Contraintes pour la table `Module`
--
ALTER TABLE `Module`
  ADD CONSTRAINT `Module_enseignant_referant` FOREIGN KEY (`enseignant_referent`) REFERENCES `Utilisateur` (`idUtilisateur`);

--
-- Contraintes pour la table `Utilisateur_has_module`
--
ALTER TABLE `Utilisateur_has_module`
  ADD CONSTRAINT `UHM_FK_idModule` FOREIGN KEY (`uhm_idModule`) REFERENCES `Module` (`idModule`),
  ADD CONSTRAINT `UHM_FK_idUtilisateur` FOREIGN KEY (`uhm_idUtilisateur`) REFERENCES `Utilisateur` (`idUtilisateur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;