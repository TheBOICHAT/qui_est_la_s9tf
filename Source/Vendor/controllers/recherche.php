<?php

use Vendor\models\controllers\ControlError;
use Vendor\models\dao\ConnexionToServer;
use Vendor\models\dao\DaoFactory;
use Vendor\models\dao\ParsingConfigFileException;
use \Vendor\models\controllers\ControlSearch;

require_once('../models/linkers/EnumRole.php');
require_once('../models/linkers/Utilisateur.php');
require_once('../models/linkers/Module.php');
require_once('../models/dao/ConnexionToServer.php');
require_once('../models/dao/UtilisateurDao.php');
require_once('../models/dao/ModuleDao.php');
require_once('../models/dao/DaoFactory.php');
require_once('../models/dao/ParsingConfigFileException.php');
require_once('../models/dao/LoginUtilisateurDejaPresentException.php');
require_once('../models/controllers/Control.php');
require_once('../models/controllers/ControlError.php');
require_once('../models/controllers/ControlSearch.php');

$PATH_VIEWS = '../views/';
$BD_CONF = '../ressources/base_donnees/bd_connect.ini';

$control = null;

try {
    $server = new ConnexionToServer($BD_CONF);
} catch (ParsingConfigFileException $e) {
    $control = new ControlError(
        $PATH_VIEWS,
        'general/erreur.php',
        'Le fichier de configuration à la base de données n\'est pas valide.',
        'Fichier : '.$BD_CONF
    );
    $control->renderView();
    die();
}

$daoFactory = new DaoFactory($server);

$controlSearch = new ControlSearch(
    $PATH_VIEWS,
    'admin/recherche.php',
    $daoFactory->getInstanceOfUtilisateurDao(),
    $daoFactory->getInstanceOfModuleDao(),
    $_POST
);

$controlSearch->doSearch();

$controlSearch->renderView();
