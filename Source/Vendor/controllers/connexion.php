<?php

use Vendor\models\controllers\ControlConnexion;
use Vendor\models\controllers\ConstructUser;
use Vendor\models\controllers\CheckFormConnexion;

require_once("../models/dao/ConnexionToServer.php");
require_once("../models/dao/DaoFactory.php");
require_once("../models/dao/UtilisateurDao.php");
require_once("../models/controllers/Control.php");
require_once("../models/controllers/ControlError.php");
require_once('../models/dao/ParsingConfigFileException.php');
require_once("../models/dao/EchecDeConnexionDBException.php");
require_once("../models/linkers/EnumRole.php");
require_once("../models/linkers/Utilisateur.php");
require_once("../models/controllers/CheckFormConnexion.php");
require_once ("../models/controllers/ConstructUser.php");
require_once ("../models/controllers/ControlConnexion.php");


$configuration = "../ressources/base_donnees/bd_connect.ini";
$PATH_VIEW = "../views/";

$buildDao= new ConstructUser($PATH_VIEW, $configuration);
$getDao = $buildDao->getUserDao();
$user = new ControlConnexion(
        $PATH_VIEW,
        $_POST['idEmail'],
        $_POST['mdp'],
        $getDao,
        "general/connexion.php",
    new CheckFormConnexion()
    );

if(is_null($buildDao->getControl()))
{
    $buildDao = $user;
}

if(isset($_POST['submit']))
{
    $user->checkUser();

}

$buildDao->renderView();
