<?php

use Vendor\models\dao\DaoFactory;
use Vendor\models\dao\ConnexionToServer;
use Vendor\models\controllers\ControlUser;
use Vendor\models\controllers\CheckFormUser;
use Vendor\models\dao\ParsingConfigFileException;
use Vendor\models\controllers\ControlError;

require_once('../models/linkers/EnumRole.php');
require_once('../models/linkers/Utilisateur.php');
require_once('../models/dao/ConnexionToServer.php');
require_once('../models/dao/DaoFactory.php');
require_once('../models/dao/UtilisateurDao.php');
require_once('../models/dao/EchecDeConnexionDBException.php');
require_once('../models/dao/ParsingConfigFileException.php');
require_once('../models/dao/LoginUtilisateurDejaPresentException.php');
require_once('../models/controllers/Control.php');
require_once('../models/controllers/ControlUser.php');
require_once('../models/controllers/ControlError.php');
require_once('../models/controllers/CheckFormUser.php');


$PATH_VIEWS = '../views/';
$BD_CONF = '../ressources/base_donnees/bd_connect.ini';

$control = null;

try {
    $server = new ConnexionToServer($BD_CONF);
} catch (ParsingConfigFileException $e) {
    $control = new ControlError(
        $PATH_VIEWS,
        'general/erreur.php',
        'Le fichier de configuration à la base de données n\'est pas valide.',
        'Fichier : '.$BD_CONF
    );
    $control->renderView();
    die();
}

$daoFactory = new DaoFactory($server);

$controlUser = new ControlUser(
    $PATH_VIEWS,
    'admin/utilisateur.php',
    $daoFactory->getInstanceOfUtilisateurDao(),
    new CheckFormUser(),
    $_POST
);

if (isset($_POST['annuler'])) {
    // Retour à la vue par défaut.
    $controlUser->resetData();
} elseif (isset($_POST['creer'])) {
    // Création d'un utilisateur.
    $controlUser->requestCreation();
} elseif (isset($_POST['modifier'])) {
    // Modification d'un utilisateur existant.
    $controlUser->requestModification();
} elseif (isset($_POST['supprimer'])) {
    // Suppression d'un utilisateur existant.
    $controlUser->requestDeletion();
} elseif (isset($_POST['id'])) {
    // Récupération des information d'un utilisateur déjà existant.
    $controlUser->fetchInformation();
}

$controlUser->renderView();
