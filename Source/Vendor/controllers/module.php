<?php

use Vendor\models\controllers\CheckFormModule;
use Vendor\models\controllers\ControlError;
use Vendor\models\dao\DaoFactory;
use Vendor\models\dao\ConnexionToServer;
use Vendor\models\controllers\ControlModule;
use Vendor\models\dao\ParsingConfigFileException;


require_once('../models/linkers/Module.php');
require_once('../models/linkers/Utilisateur.php');
require_once('../models/linkers/EnumRole.php');
require_once('../models/dao/ConnexionToServer.php');
require_once('../models/dao/DaoFactory.php');
require_once('../models/controllers/ControlModule.php');
require_once('../models/dao/ModuleDao.php');
require_once('../models/dao/UtilisateurDao.php');
require_once('../models/controllers/CheckFormModule.php');

$PATH_VIEWS = '../views/';
$BD_CONF = '../ressources/base_donnees/bd_connect.ini';

$control = null;

try {
    $server = new ConnexionToServer($BD_CONF);
} catch (ParsingConfigFileException $e) {
    $control = new ControlError(
        $PATH_VIEWS,
        'general/erreur.php',
        'Le fichier de configuration à la base de données n\'est pas valide.',
        'Fichier : '.$BD_CONF
    );
    $control->renderView();
    die();
}

$daoFactory = new DaoFactory($server);

$controlerModule = new ControlModule(
    $daoFactory->getInstanceOfModuleDao(),
    $daoFactory->getInstanceOfUtilisateurDao(),
    new CheckFormModule(),
    $PATH_VIEWS,
    'admin/module.php',
    $_POST
);

if (isset($_POST['annuler'])) {
    // Retour à la vue par défaut.
    $controlerModule->resetData();
} elseif (isset($_POST['creer'])) {
    // Création d'un module
    $controlerModule->requestCreation();
} elseif (isset($_POST['modifier'])) {
    // Modification d'un module existant
    $controlerModule->requestModification();
} elseif (isset($_POST['supprimer'])) {
    // Suppression d'un module existant.
    $controlerModule->requestDeletion();
} elseif (isset($_POST['id'])) {
    // Récupération des information d'un module déjà existant.
    $controlerModule->fetchInformation();
}

$controlerModule->renderView();

