<?php
namespace Vendor\models\controllers\tests\units;

use \atoum;
use Vendor\models\controllers;
use Vendor\models\linkers\EnumRole;

class CheckFormUser extends atoum
{
    public function testCheckFormErrorsAllEmpty()
    {
        $dataPost = array(
            'nom' => null,
            'prenom' => null,
            'email' => null,
            'role' => null,
            'id' => -1,
        );

        $chkFormUser = new controllers\CheckFormUser();

        $errors = $chkFormUser->checkFormErrors($dataPost['nom'], $dataPost['prenom'], $dataPost['email'], $dataPost['role']);

        $this
            ->string($errors['nom'])
            ->isEqualTo("Veuillez renseigner le nom.")
            ->string($errors['prenom'])
            ->isEqualTo("Veuillez renseigner le prénom.")
            ->string($errors['email'])
            ->isEqualTo("Veuillez renseigner l'adresse email.")
            ->string($errors['role'])
            ->isEqualTo("Veuillez choisir un rôle.");
    }

    public function testCheckFormErrorUnkownRole()
    {
        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => -1,
            'id' => -1,
        );

        $chkFormUser = new controllers\CheckFormUser();

        // Rôle inconnue, dans les négatif

        $errors = $chkFormUser->checkFormErrors($dataPost['nom'], $dataPost['prenom'], $dataPost['email'], $dataPost['role']);
        $this->string($errors['role'])->isEqualTo("Le rôle choisit est inconnu.");

        // Rôle inconnue, trop grand

        $errors = $chkFormUser->checkFormErrors($dataPost['nom'], $dataPost['prenom'], $dataPost['email'], EnumRole::Personnel + 1);
        $this->string($errors['role'])->isEqualTo("Le rôle choisit est inconnu.");
    }

    public function testCheckIdErrorsIdOK()
    {
        $chkFormUser = new controllers\CheckFormUser();

        $status = $chkFormUser->checkIdErrors(0);

        $this
            ->array($status)
            ->isEmpty();
    }

    public function testCheckIdErrorsIdKO()
    {
        $chkFormUser = new controllers\CheckFormUser();

        $status = $chkFormUser->checkIdErrors(-1);

        $this
            ->boolean($status['success'])
            ->isFalse()
            ->string($status['msg'])
            ->isEqualTo("Identifiant utilisateur invalide.");
    }
}