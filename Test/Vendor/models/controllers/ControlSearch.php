<?php
namespace Vendor\models\controllers\tests\units;

use \atoum;
use Vendor\models\controllers;
use Vendor\models\linkers\EnumRole;
use Vendor\models\linkers\Module;
use Vendor\models\linkers\Utilisateur;

require_once("Source/Vendor/models/dao/UtilisateurDao.php");
require_once("Source/Vendor/models/dao/ModuleDao.php");
require_once("Source/Vendor/models/linkers/Utilisateur.php");
require_once("Source/Vendor/models/linkers/Module.php");

class ControlSearch extends atoum
{
    public function testConstructOK()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array('recherche' => 'pouet');

        $userDao = $this->newMockInstance('UtilisateurDao');
        $moduleDao = $this->newMockInstance('ModuleDao');

        $ctrlSearch = new controllers\ControlSearch($pathViews, $view, $userDao, $moduleDao, $dataPost);

        $this
            ->string($ctrlSearch->getSearch())
            ->isEqualTo($dataPost['recherche']);
    }

    public function testConstructKO()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $moduleDao = $this->newMockInstance('ModuleDao');

        $ctrlSearch = new controllers\ControlSearch($pathViews, $view, $userDao, $moduleDao, $dataPost);

        $this
            ->variable($ctrlSearch->getSearch())
            ->isNull();
    }

    public function testDoSearchUserAndModule()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array('recherche' => 'pouet');

        $user = new Utilisateur('test1', 'test2', EnumRole::Administrateur);
        $user->setId(1);
        $module = new Module();
        $module->setLibelle("Pouet test");
        $module->setIdModule(1);

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->findByName = array($user);
        $moduleDao = $this->newMockInstance('ModuleDao');
        $this->calling($moduleDao)->findByName = array($module);

        $ctrlSearch = new controllers\ControlSearch($pathViews, $view, $userDao, $moduleDao, $dataPost);

        $ctrlSearch->doSearch();

        $this
            ->array($ctrlSearch->getStatus())
                ->boolean['success']->isTrue()
                ->string['msg']->isEqualTo('Résultat(s) pour la recherche : "' . $dataPost['recherche'] . '"')
            ->array($ctrlSearch->getUserResults())
                ->contains($user)
            ->array($ctrlSearch->getModResults())
                ->contains($module);
    }

    public function testDoSearchOnlyUser()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array('recherche' => 'pouet');

        $user = new Utilisateur('test1', 'test2', EnumRole::Administrateur);
        $user->setId(1);

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->findByName = array($user);
        $moduleDao = $this->newMockInstance('ModuleDao');
        $this->calling($moduleDao)->findByName = array();

        $ctrlSearch = new controllers\ControlSearch($pathViews, $view, $userDao, $moduleDao, $dataPost);

        $ctrlSearch->doSearch();

        $this
            ->array($ctrlSearch->getStatus())
                ->boolean['success']->isTrue()
                ->string['msg']->isEqualTo('Résultat(s) pour la recherche : "' . $dataPost['recherche'] . '"')
            ->array($ctrlSearch->getUserResults())
                ->contains($user)
            ->array($ctrlSearch->getModResults())
                ->isEmpty();
    }

    public function testDoSearchOnlyModule()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array('recherche' => 'pouet');

        $module = new Module();
        $module->setLibelle("Pouet test");
        $module->setIdModule(1);

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->findByName = array();
        $moduleDao = $this->newMockInstance('ModuleDao');
        $this->calling($moduleDao)->findByName = array($module);

        $ctrlSearch = new controllers\ControlSearch($pathViews, $view, $userDao, $moduleDao, $dataPost);

        $ctrlSearch->doSearch();

        $this
            ->array($ctrlSearch->getStatus())
                ->boolean['success']->isTrue()
                ->string['msg']->isEqualTo('Résultat(s) pour la recherche : "' . $dataPost['recherche'] . '"')
            ->array($ctrlSearch->getUserResults())
                ->isEmpty()
            ->array($ctrlSearch->getModResults())
                ->contains($module);
    }

    public function testDoSearchNoResult()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array('recherche' => 'pouet');

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->findByName = array();
        $moduleDao = $this->newMockInstance('ModuleDao');
        $this->calling($moduleDao)->findByName = array();

        $ctrlSearch = new controllers\ControlSearch($pathViews, $view, $userDao, $moduleDao, $dataPost);

        $ctrlSearch->doSearch();

        $this
            ->array($ctrlSearch->getStatus())
                ->boolean['success']->isFalse()
                ->string['msg']->isEqualTo('Aucun résultat pour la recherche : "' . $dataPost['recherche'] . '"')
            ->array($ctrlSearch->getUserResults())
                ->isEmpty()
            ->array($ctrlSearch->getModResults())
                ->isEmpty();
    }

    public function testDoSearchNothingSearched()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $moduleDao = $this->newMockInstance('ModuleDao');

        $ctrlSearch = new controllers\ControlSearch($pathViews, $view, $userDao, $moduleDao, $dataPost);

        $ctrlSearch->doSearch();

        $status = $ctrlSearch->getStatus();

        $this
            ->variable($status['success'])
                ->isNull()
            ->string($status['msg'])
                ->isEqualTo('Veuillez procéder à une recherche.');
    }

    public function testRenderView()
    {
        $pathViews = 'Source/Vendor/views/';
        $view = 'admin/recherche.php';

        $dataPost = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $moduleDao = $this->newMockInstance('ModuleDao');

        $ctrlSearch = new controllers\ControlSearch($pathViews, $view, $userDao, $moduleDao, $dataPost);

        // les méthodes ob_*() permettent d'obtenir
        // la vue sous forme de chaîne de caractères.
        ob_start();
        $ctrlSearch->renderView();
        $html = ob_get_clean();

        $this
            ->string($html)
            ->contains('Recherche');
    }
}