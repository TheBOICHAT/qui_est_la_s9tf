<?php

namespace Vendor\models\controllers\tests\units;

use \atoum;
use Vendor\models\controllers;


class CheckFormConnexion extends atoum
{

    public function testNOError()
    {
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';

        $checkError = new controllers\CheckFormConnexion();
        $errors = $checkError->checkConxErrors($login, $paswd);

        $this
            ->array($errors)
            ->isEmpty();
    }
    public function testErrorLogin()
    {
        $login = null;
        $paswd = 'yassine';

        $checkError = new controllers\CheckFormConnexion();
        $errors = $checkError->checkConxErrors($login, $paswd);

        $this
            ->string($errors['login'])
            ->isEqualTo("Veuillez renseigner votre Email.");
    }
    public function testErrorPassword()
    {
        $login = "yas.lou@gm.com";
        $paswd = null;

        $checkError = new controllers\CheckFormConnexion();
        $errors = $checkError->checkConxErrors($login, $paswd);

        $this
            ->string($errors['mdp'])
            ->isEqualTo("Veuillez renseigner votre mot de passe.");
    }
    public function testErrorAllEmpty()
    {
        $login = null;
        $paswd = null;

        $checkError = new controllers\CheckFormConnexion();
        $errors = $checkError->checkConxErrors($login, $paswd);

        $this
            ->string($errors['login'])
            ->isEqualTo("Veuillez renseigner votre Email.")
            ->string($errors['mdp'])
            ->isEqualTo("Veuillez renseigner votre mot de passe.");
    }
}