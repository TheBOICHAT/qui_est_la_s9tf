<?php
namespace Vendor\models\controllers\tests\units;

use \atoum;
use Vendor\models\controllers;

class CheckFormModule extends atoum
{
    public function testcheckFormErrorsAllEmpty()
    {
        $dataPost = array(
            'nom' => null,
            'ensRef' => null,
            'idModule' => -1,
        );

        $chkFormModule = new controllers\CheckFormModule();

        $errors = $chkFormModule->checkFormErrors($dataPost['nom'], $dataPost['ensRef']);
        $this
            ->string($errors['nom'])
            ->isEqualTo("Veuillez renseigner le libellé de module.")
            ->string($errors['ensRef'])
            ->isEqualTo("Veuillez choisir un enseignant réferent");
    }


    public function testCheckIdErrorsIdOK()
    {
        $chkFormModule = new controllers\CheckFormModule();

        $status = $chkFormModule->checkIdErrors(0);

        $this
            ->array($status)
            ->isEmpty();
    }

    public function testCheckIdErrorsIdKO()
    {
        $chkFormModule = new controllers\CheckFormModule();

        $status = $chkFormModule->checkIdErrors(-1);

        $this
            ->boolean($status['success'])
            ->isFalse()
            ->string($status['msg'])
            ->isEqualTo("Identifiant module invalide.");
    }
}