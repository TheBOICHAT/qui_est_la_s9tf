<?php
namespace Vendor\models\controllers\tests\units;

use \atoum;
use Vendor\models\controllers;
use Vendor\models\dao\EchecDeConnexionDBException;
use Vendor\models\linkers\Module;
use Vendor\models\linkers\Utilisateur;

// require_once("Source/Vendor/models/dao/ModuleDao.php");

class ControlModule extends atoum
{

    public function testConstructOK()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'ensRef' => 1,
            'id' => 1,
        );

        $moduleDao = $this->newMockInstance('ModuleDao');
        $utilisateurDao = $this->newMockInstance('UtilisateurDao');
        $chkFormModule = $this->newMockInstance('CheckFormModule');

        $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

        $this
            ->string($controlerModule->getNom())
            ->isEqualTo($dataPost['nom'])
            ->integer($controlerModule->getEnseignantReferant())
            ->isEqualTo($dataPost['ensRef'])
            ->integer($controlerModule->getIdModule())
            ->isEqualTo($dataPost['id']);
    }
    public function testDefineValue()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'ensRef' => 1,
            'id' => 1,
        );

        $moduleDao = $this->newMockInstance('ModuleDao');
        $utilisateurDao = $this->newMockInstance('UtilisateurDao');
        $chkFormModule = $this->newMockInstance('CheckFormModule');

        $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

        $this
            ->integer($controlerModule->getIdModule())
            ->isEqualTo($dataPost['id'])
            ->integer($controlerModule->getEnseignantReferant())
            ->isEqualTo($dataPost['ensRef']);

        // ---

        $dataPost['ensRef'] = 'pouet';
        $dataPost['id'] = 'pouet';

        $moduleDao = $this->newMockInstance('ModuleDao');
        $utilisateurDao = $this->newMockInstance('UtilisateurDao');
        $chkFormModule = $this->newMockInstance('CheckFormModule');

        $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

        $this
            ->integer($controlerModule->getIdModule())
            ->isEqualTo(-1);

        $this
            ->integer($controlerModule->getEnseignantReferant())
            ->isEqualTo(-1);

        // ---

        $dataPost['ensRef'] = null;
        $dataPost['id'] = null;

        $moduleDao = $this->newMockInstance('ModuleDao');
        $utilisateurDao = $this->newMockInstance('UtilisateurDao');
        $chkFormModule = $this->newMockInstance('CheckFormModule');

        $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

        $this->boolean(empty($controlerModule->getEnseignantReferant()))->isTrue();
        $this->boolean(empty($controlerModule->getIdModule()))->isTrue();
    }

        public function testResetData()
        {
            $pathViews = '';
            $view = '';

            $dataPost = array(
                'nom' => 'module',
                'ensRef' => 0,
                'id' => 1,
            );

            $moduleDao = $this->newMockInstance('ModuleDao');
            $utilisateurDao = $this->newMockInstance('UtilisateurDao');
            $chkFormModule = $this->newMockInstance('CheckFormModule');

            $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

            $controlerModule->resetData();

            $this
                ->string($controlerModule->getNom())
                ->isEmpty()
                ->integer($controlerModule->getEnseignantReferant())
                ->isEqualTo(-1)
                ->integer($controlerModule->getIdModule())
                ->isEqualTo(-1);
        }

        public function testRequestCreationOK()
        {
            $pathViews = '';
            $view = '';

            $dataPost = array(
                'nom' => 'module',
                'ensRef' => 1,
                'id' => 1,
            );

            $chkFormModule = $this->newMockInstance('CheckFormModule');
            $this->calling($chkFormModule)->checkFormErrors = array();

            $moduleDao = $this->newMockInstance('ModuleDao');
            $utilisateurDao = $this->newMockInstance('UtilisateurDao');
            $this->calling($moduleDao)->add = true;

            $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

            $controlerModule->requestCreation();

            $status = $controlerModule->getStatus();

            $this
                ->boolean($status['success'])
                ->isTrue()
                ->string($status['msg'])
                ->isEqualTo("Module créée avec succès !");
        }

        public function testRequestCreationKO()
        {
            $chkFormModule = $this->newMockInstance('CheckFormModule');
            $this->calling($chkFormModule)->checkFormErrors = array();

            $moduleDao = $this->newMockInstance('ModuleDao');
            $utilisateurDao = $this->newMockInstance('UtilisateurDao');
            $this->calling($moduleDao)->add = function() {
                throw new EchecDeConnexionDBException('');
            };

            $pathViews = '';
            $view = '';

            $dataPost = array(
                'nom' => 'module',
                'ensRef' => 1,
                'id' => 1,
            );

            $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

            $controlerModule->requestCreation();

            $status = $controlerModule->getStatus();

            $this
                ->boolean($status['success'])
                ->isFalse()
                ->string($status['msg'])
                ->isEqualTo("Erreur lors de la création du Module");
        }

          public function testFetchInformationOK()
           {
               $pathViews = '';
               $view = '';

               $dataPost = array(
                   'nom' => 'module',
                   'ensRef' => 1,
                   'id' => 1,
               );

               $moduleFetched = new Module();
               $moduleFetched->setIdModule(1);
               $moduleFetched->setLibelle("test");
               $moduleFetched->setEnseignantReferant(1);
               $utilisateurFetched = new Utilisateur();
               $utilisateurFetched->setId(1);
               $utilisateurFetched->setNom("XX");
               $utilisateurFetched->setPrenom("YY");

               $chkFormModule = $this->newMockInstance('CheckFormModule');
               $this->calling($chkFormModule)->checkFormErrors = array();

               $moduleDao = $this->newMockInstance('ModuleDao');
               $utilisateurDao = $this->newMockInstance('UtilisateurDao');
               $this->calling($moduleDao)->getById = $moduleFetched;
               $this->calling($utilisateurDao)->getUserById = $utilisateurFetched;

               $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);
               $controlerModule->fetchInformation();
                $this
                    ->string($controlerModule->getNom())
                    ->isEqualTo($moduleFetched->getLibelle())
                   ->integer($controlerModule->getEnseignantReferant())
                   ->isEqualTo($moduleFetched->getEnseignantReferant())
                   ->integer($controlerModule->getIdModule())
                   ->isEqualTo($moduleFetched->getIdModule());
           }

              public function testFetchInformationKO()
              {
                  $pathViews = '';
                  $view = '';

                  $dataPost = array( );

                  $chkFormModule = $this->newMockInstance('CheckFormModule');
                  $this->calling($chkFormModule)->checkFormErrors = array();

                  $moduleDao = $this->newMockInstance('ModuleDao');
                  $utilisateurDao = $this->newMockInstance('UtilisateurDao');
                  $this->calling($moduleDao)->getById = function () {
                      throw new EchecDeConnexionDBException('');
                  };

                  $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

                  $controlerModule->fetchInformation();

                  $status = $controlerModule->getStatus();

                  $this
                      ->boolean($status['success'])
                      ->isFalse()
                      ->string($status['msg'])
                      ->isEqualTo("Erreur lors de l'obtention des informations du module ().");
              }


              public function testRequestModificationOK()
              {
                  $pathViews = '';
                  $view = '';

                  $dataPost = array();

                  $chkFormModule = $this->newMockInstance('CheckFormModule');
                  $this->calling($chkFormModule)->checkFormErrors = array();
                  $this->calling($chkFormModule)->checkIdErrors = array();

                  $moduleDao = $this->newMockInstance('ModuleDao');
                  $utilisateurDao = $this->newMockInstance('UtilisateurDao');
                  $this->calling($moduleDao)->update = true;

                  $controlerModule = new controllers\ControlModule($moduleDao,$utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

                  $controlerModule->requestModification();

                  $status = $controlerModule->getStatus();

                  $this
                      ->boolean($status['success'])
                      ->isTrue()
                      ->string($status['msg'])
                      ->isEqualTo("Module modifié avec succès !");
              }

            public function testRequestModificationKO()
              {
                  $pathViews = 'pathView';
                  $view = '';

                  $dataPost = array();

                  $chkFormModule = $this->newMockInstance('CheckFormModule');
                  $this->calling($chkFormModule)->checkFormErrors = array();
                  $this->calling($chkFormModule)->checkIdErrors = array();

                  $moduleDao = $this->newMockInstance('ModuleDao');
                  $utilisateurDao = $this->newMockInstance('UtilisateurDao');
                  $this->calling($moduleDao)->update = function () {
                      throw new EchecDeConnexionDBException('');
                  };

                  $controlerModule = new controllers\ControlModule($moduleDao, $utilisateurDao,$chkFormModule, $pathViews, $view, $dataPost);

                  $controlerModule->requestModification();

                  $status = $controlerModule->getStatus();

                  $this
                      ->boolean($status['success'])
                      ->isFalse()
                      ->string($status['msg'])
                      ->isEqualTo("Erreur lors de la modéfication du module !");
              }

                 public function testRequestDeletionOK()
                 {
                     $pathViews = '';
                     $view = '';

                     $dataPost = array();

                     $chkFormModule = $this->newMockInstance('CheckFormModule');
                     $this->calling($chkFormModule)->checkIdErrors = array();

                     $moduleDao = $this->newMockInstance('ModuleDao');
                     $utilisateurDao = $this->newMockInstance('UtilisateurDao');
                     $this->calling($moduleDao)->delete = true;

                     $controlerModule = new controllers\ControlModule($moduleDao, $utilisateurDao, $chkFormModule, $pathViews, $view, $dataPost);

                     $controlerModule->requestDeletion();

                     $status = $controlerModule->getStatus();

                     $this
                         ->boolean($status['success'])
                         ->isTrue()
                         ->string($status['msg'])
                         ->isEqualTo("Module supprimé avec succès !");
                 }

       public function testRequestDeletionKO()
         {
             $pathViews = 'testPath';
             $view = '';

             $dataPost = array(
                 'nom' => 'test1',
                 'ensRef' => 1,
                 'id' => 1,
             );

             $chkFormModule = $this->newMockInstance('CheckFormModule');
             $this->calling($chkFormModule)->checkIdErrors = array();

             $moduleDao = $this->newMockInstance('ModuleDao');
             $utilisateurDao = $this->newMockInstance('UtilisateurDao');
             $this->calling($moduleDao)->delete = function () {
                 throw new EchecDeConnexionDBException('');
             };

             $controlerModule = new controllers\ControlModule($moduleDao, $utilisateurDao,$chkFormModule, $pathViews, $view, $dataPost);

             $controlerModule->requestDeletion();

             $status = $controlerModule->getStatus();

             $this
                 ->boolean($status['success'])
                 ->isFalse()
                 ->string($status['msg'])
                 ->isEqualTo("Erreur lors de la suppression de module");
         }
}