<?php
namespace Vendor\models\controllers\tests\units;

use \atoum;
use Vendor\models\controllers;
use Vendor\models\dao\EchecDeConnexionDBException;
use Vendor\models\linkers\EnumRole;
use Vendor\models\linkers\Utilisateur;

require_once("Source/Vendor/models/dao/UtilisateurDao.php");
require_once("Source/Vendor/models/linkers/Utilisateur.php");

class ControlUser extends atoum
{
    public function testConstructOK()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Administrateur,
            'id' => 1,
        );

        $userDao = $this->newMockInstance('UtilisateurDao');
        $chkFormUser = $this->newMockInstance('CheckFormUser');

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $this
            ->string($ctrlUser->getLastName())
            ->isEqualTo($dataPost['nom'])
            ->string($ctrlUser->getFirstName())
            ->isEqualTo($dataPost['prenom'])
            ->string($ctrlUser->getEmail())
            ->isEqualTo($dataPost['email'])
            ->integer($ctrlUser->getRole())
            ->isEqualTo($dataPost['role'])
            ->integer($ctrlUser->getId())
            ->isEqualTo($dataPost['id']);
    }

    public function testDefineValue()
    {
        $pathViews = 'pouet';
        $view = 'pouet';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Administrateur,
            'id' => 1,
        );

        $userDao = $this->newMockInstance('UtilisateurDao');
        $chkFormUser = $this->newMockInstance('CheckFormUser');

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $this
            ->integer($ctrlUser->getRole())
            ->isEqualTo($dataPost['role'])
            ->integer($ctrlUser->getId())
            ->isEqualTo($dataPost['id']);

        // ---

        $dataPost['role'] = 'pouet';
        $dataPost['id'] = 'pouet';

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $this
            ->integer($ctrlUser->getRole())
            ->isEqualTo(-1);

        $this
            ->integer($ctrlUser->getId())
            ->isEqualTo(-1);

        // ---

        $dataPost['role'] = null;
        $dataPost['id'] = null;

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $this->boolean(empty($ctrlUser->getRole()))->isTrue();
        $this->boolean(empty($ctrlUser->getId()))->isTrue();
    }

    public function testResetData()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Administrateur,
            'id' => 1,
        );

        $userDao = $this->newMockInstance('UtilisateurDao');
        $chkFormUser = $this->newMockInstance('CheckFormUser');

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->resetData();

        $this
            ->string($ctrlUser->getLastName())
            ->isEmpty()
            ->string($ctrlUser->getFirstName())
            ->isEmpty()
            ->string($ctrlUser->getEmail())
            ->isEmpty()
            ->integer($ctrlUser->getRole())
            ->isEqualTo(-1)
            ->integer($ctrlUser->getId())
            ->isEqualTo(-1);
    }

    public function testRequestCreationOK()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Administrateur,
            'id' => 1,
        );

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkFormErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->add = true;

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->requestCreation();

        $status = $ctrlUser->getStatus();

        $this
            ->boolean($status['success'])
            ->isTrue()
            ->string($status['msg'])
            ->isEqualTo("Utilisateur créée avec succès !");
    }

    public function testRequestCreationPasswordInitialNotEtudiant()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Administrateur,
        );

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkFormErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');

        $mdp_full = '123456789';
        $mdp_expected = '1234';

        $this->function->rand = $mdp_full;

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->requestCreation();

        $this
            ->string($ctrlUser->getMdp())
            ->isEqualTo($mdp_expected);
    }

    public function testRequestCreationPasswordInitialIsEtudiant()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Etudiant,
        );

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkFormErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->requestCreation();

        $this
            ->variable($ctrlUser->getMdp())
            ->isNull();
    }

    public function testRequestCreationKO()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Administrateur,
            'id' => 1,
        );

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkFormErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->add = function() {
            throw new EchecDeConnexionDBException('');
        };

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->requestCreation();

        $status = $ctrlUser->getStatus();

        $this
            ->boolean($status['success'])
            ->isFalse()
            ->string($status['msg'])
            ->isEqualTo("Erreur lors de la création de l'utilisateur ().");
    }

    public function testFetchInformationOK()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array();

        $userFetched = new Utilisateur('test1', 'test2', 'test3');

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkFormErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->getUserById = $userFetched;

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->fetchInformation();

        $this
            ->string($ctrlUser->getLastName())
            ->isEqualTo($userFetched->getNom())
            ->string($ctrlUser->getFirstName())
            ->isEqualTo($userFetched->getPrenom());
    }

    public function testFetchInformationKO()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array();

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkFormErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->getUserById = function () {
            throw new EchecDeConnexionDBException('');
        };

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->fetchInformation();

        $status = $ctrlUser->getStatus();

        $this
            ->boolean($status['success'])
            ->isFalse()
            ->string($status['msg'])
            ->isEqualTo("Erreur lors de l'obtention des informations de l'utilisateur ().");
    }


    public function testRequestModificationOK()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array();

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkFormErrors = array();
        $this->calling($chkFormUser)->checkIdErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->update = true;

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->requestModification();

        $status = $ctrlUser->getStatus();

        $this
            ->boolean($status['success'])
            ->isTrue()
            ->string($status['msg'])
            ->isEqualTo("Informations modifiées avec succès !");
    }

    public function testRequestModificationKO()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array();

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkFormErrors = array();
        $this->calling($chkFormUser)->checkIdErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->update = function () {
            throw new EchecDeConnexionDBException('');
        };

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->requestModification();

        $status = $ctrlUser->getStatus();

        $this
            ->boolean($status['success'])
            ->isFalse()
            ->string($status['msg'])
            ->isEqualTo("Erreur lors de la modification des informations ().");
    }

    public function testRequestDeletionOK()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Administrateur,
            'id' => 1,
        );

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkIdErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->delete = true;

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->requestDeletion();

        $status = $ctrlUser->getStatus();

        $this
            ->boolean($status['success'])
            ->isTrue()
            ->string($status['msg'])
            ->isEqualTo("Utilisateur supprimé avec succès !");
    }

    public function testRequestDeletionKO()
    {
        $pathViews = '';
        $view = '';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Administrateur,
            'id' => 1,
        );

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $this->calling($chkFormUser)->checkIdErrors = array();

        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->delete = function () {
            throw new EchecDeConnexionDBException('');
        };

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        $ctrlUser->requestDeletion();

        $status = $ctrlUser->getStatus();

        $this
            ->boolean($status['success'])
            ->isFalse()
            ->string($status['msg'])
            ->isEqualTo("Erreur lors de la suppression de l'utilisateur ().");
    }

    public function testRenderView()
    {
        $pathViews = 'Source/Vendor/views/';
        $view = 'admin/utilisateur.php';

        $dataPost = array(
            'nom' => 'test1',
            'prenom' => 'test2',
            'email' => 'test3',
            'role' => EnumRole::Administrateur,
            'id' => 1,
        );

        $chkFormUser = $this->newMockInstance('CheckFormUser');
        $userDao = $this->newMockInstance('UtilisateurDao');

        $ctrlUser = new controllers\ControlUser($pathViews, $view, $userDao, $chkFormUser, $dataPost);

        // les méthodes ob_*() permettent d'obtenir
        // la vue sous forme de chaîne de caractères.
        ob_start();
        $ctrlUser->renderView();
        $html = ob_get_clean();

        $this
            ->string($html)
            ->contains('Utilisateur');
    }
}
