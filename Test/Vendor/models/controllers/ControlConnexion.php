<?php
namespace Vendor\models\controllers\tests\units;

use \atoum;
use mageekguy\atoum\mock\controller\linker;
use Vendor\models\controllers;
use Vendor\models\linkers;


class ControlConnexion extends atoum
{
    //test constructeur
    public function testConstructOk()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $usrDao = $this->newMockInstance('UtilisateurDao');
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $usrDao, $view,$chekConx);

        $this
            ->string($testUser->getLogin())
            ->isEqualTo($login)
            ->string($testUser->getPassword())
            ->isEqualTo($paswd)
            ->string($testUser->getPathView())
            ->isEqualTO($path)
            ->string($testUser->getView())
            ->isEqualTO($view);
    }
    //cas utilisateur non exist
    public function testVerifyEmail_User_Not_Exist()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $userDao = $this->newMockInstance('UtilisateurDao');
        $this->calling($userDao)->findByEmail = null;
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $userDao, $view,$chekConx);

        $this
            ->boolean($testUser->verifyEmail())
            ->isEqualTo(false)
            ->string($testUser->getErrors()['verifyMail'])
            ->isEqualTo("Utilisteur Inconnu renseigner un nouveau email");
    }
    //cas utilisateur existe avec mot de passe correct
    public function testVerifyEmail_OK_Pass()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';

        $user = new linkers\Utilisateur("yassine", "lounani", "admin");
        $hash_pass =password_hash("yassine", PASSWORD_DEFAULT);
        $user->setMotDePass($hash_pass);
        $userDao = $this->newMockInstance('UtilisateurDao');
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $userDao, $view,$chekConx);
        $this->calling($userDao)->findByEmail = $user;

        $this
            ->boolean($testUser->verifyEmail())
            ->isEqualTo(true);


        //test cas user
    }
    //cas utilisateur existe mais mot de pass incorrecte
    public function testVerifyEmail_KO_PASS()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';

        $user = new linkers\Utilisateur("yassine", "lounani", "admin");
        $hash_pass =password_hash("hakim", PASSWORD_DEFAULT);
        $user->setMotDePass($hash_pass);
        $userDao = $this->newMockInstance('UtilisateurDao');
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $userDao, $view,$chekConx);
        $this->calling($userDao)->findByEmail = $user;

        $this
            ->boolean($testUser->verifyEmail())
            ->isEqualTo(false)
            ->string($testUser->getErrors()['verifyMDP'])
            ->isEqualTo("Votre mot de passe est incorect !");
    }

    public function testFindUserOK()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';
        $chekConx = 'error';
        $user = new linkers\Utilisateur("yassine", "lounani", "admin");
        $hash_pass =password_hash("yassine", PASSWORD_DEFAULT);
        $user->setMotDePass($hash_pass);
        $userDao = $this->newMockInstance('UtilisateurDao');
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $this->calling($chekConx)->checkConxErrors = array();
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $userDao, $view,$chekConx);
        $this->calling($userDao)->findByEmail = $user;

        $this
            ->boolean($testUser->findUser())
            ->isEqualTo(true);

    }
    public function testFindUserKO()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';
        $userDao = $this->newMockInstance('UtilisateurDao');
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $this->calling($chekConx)->checkConxErrors = array();
        $this->calling($userDao)->findByEmail = null;
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $userDao, $view,$chekConx);

        $this
            ->boolean($testUser->findUser())
            ->isEqualTo(false);

    }

    public function test_is_array_empty_OK()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';
        $errorVide = array();
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $userDao = $this->newMockInstance('UtilisateurDao');
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $userDao, $view, $chekConx);

        $this
            ->boolean($testUser->is_array_empty($errorVide))
            ->isEqualTo(true);
    }
    public function test_is_array_empty_KO()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';
        $errorPlein = array("login" => "karime", "mdp" => "karima");
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $userDao = $this->newMockInstance('UtilisateurDao');
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $userDao, $view,$chekConx);

        $this
            ->boolean($testUser->is_array_empty($errorPlein))
            ->isEqualTo(false);
    }
    public function testCheckOK()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';
        $user = new linkers\Utilisateur("yassine", "lounani", "admin");
        $hash_pass =password_hash("yassine", PASSWORD_DEFAULT);
        $user->setMotDePass($hash_pass);
        $userDao = $this->newMockInstance('UtilisateurDao');
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $this->calling($chekConx)->checkConxErrors = array();
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $userDao, $view,$chekConx);
        $this->calling($userDao)->findByEmail = $user;

        $this
            ->boolean($testUser->checkUser())
            ->isEqualTo(true);

    }
    public function testCheckKO()
    {
        $path = 'path';
        $login = 'yas.zin@gmail.com';
        $paswd = 'yassine';
        $view = 'view';
        $user = new linkers\Utilisateur("paul", "toto", "admin");
        $hash_pass =password_hash("paul", PASSWORD_DEFAULT);
        $user->setMotDePass($hash_pass);
        $userDao = $this->newMockInstance('UtilisateurDao');
        $chekConx = $this->newMockInstance('ChekFormConnexion');
        $this->calling($chekConx)->checkConxErrors = array();
        $testUser = new controllers\ControlConnexion($path, $login, $paswd, $userDao, $view,$chekConx);
        $this->calling($userDao)->findByEmail = $user;

        $this
            ->boolean($testUser->checkUser())
            ->isEqualTo(false);

    }

}
?>