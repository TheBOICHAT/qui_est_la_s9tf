<?php
namespace Vendor\models\controllers\tests\units;

use \atoum;
use Vendor\models\controllers;

class ControlError extends atoum
{
    public function testRenderView()
    {
        $pathViews = 'Source/Vendor/views/';
        $view = 'general/erreur.php';
        $message = 'Test message';
        $detail = 'Test detail';

        $ctrlError = new controllers\ControlError(
            $pathViews,
            $view,
            $message,
            $detail
        );

        ob_start();
        $ctrlError->renderView();
        $html = ob_get_clean();

        $this
            ->string($html)
            ->contains($message)
            ->contains($detail);
    }
}