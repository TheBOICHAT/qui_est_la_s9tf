<?php
namespace Vendor\models\dao\tests\units;

use atoum;

use PhpParser\Node\Expr\Array_;
use Vendor\models\dao\ConnexionToServer;
use Vendor\models\dao\DaoFactory;
use Vendor\models\linkers\EnumRole;
use Vendor\models\linkers\Module;

/**
 * @engine isolate
 */
class ModuleDao extends atoum
{
    private $factory;
    private $db_file_config="Source/Vendor/ressources/base_donnees/bd_connect.ini";
    private $dao;
    private $testTable="testModule";
    private $dbConnect;
    private $lastModuleId;

    public function setUp(){
        $this->dbConnect=new ConnexionToServer($this->db_file_config);
        $this->factory=new DaoFactory($this->dbConnect);
        $this->dao=$this->factory->getInstanceOfModuleDao();
        $this->dao->setTable($this->testTable);
    }

    public function afterTestMethod($method){
        switch ($method){
            case 'testAddModule':
                $testedModule = new Module("","");
                $testedModule->setIdModule($this->lastModuleId);
                $this->dao->delete($testedModule);
                break;
            case 'testGetById':
                $testedModule = new Module("","");
                $testedModule->setIdModule($this->lastModuleId);
                $this->dao->delete($testedModule);
                break;
            case 'testUpdate':
                $testedModule = new Module("","");
                $testedModule->setIdModule($this->lastModuleId);
                $this->dao->delete($testedModule);
                break;
            case 'testSearchByLibelle':
                $testedModule = new Module("","");
                $testedModule->setIdModule($this->lastModuleId[0]);
                $this->dao->delete($testedModule);
                $testedModule->setIdModule($this->lastModuleId[1]);
                $this->dao->delete($testedModule);
                $testedModule->setIdModule($this->lastModuleId[2]);
                $this->dao->delete($testedModule);
                break;
            case 'testGetAll':
                $testedModule = new Module("","");
                $testedModule->setIdModule($this->lastModuleId[0]);
                $this->dao->delete($testedModule);
                $testedModule->setIdModule($this->lastModuleId[1]);
                $this->dao->delete($testedModule);
                $testedModule->setIdModule($this->lastModuleId[2]);
                $this->dao->delete($testedModule);
                break;
        }
    }

    public function  testSearchByLibelle(){
        $this->setup();
        $mod1 = new Module("test",1);
        $mod2 = new Module("ijtestou",3);
        $mod3 = new Module("rienavoir",5);
        $mod = array();
        $this->lastModuleId = array();
        $this
            ->given($libelle="test")
            ->and(
                array_push($this->lastModuleId,$this->dao->add($mod3)),
                array_push($this->lastModuleId,$this->dao->add($mod2)),
                array_push($this->lastModuleId,$this->dao->add($mod1)),
                $mod = $this->dao->findByName($libelle)
            )
            ->then
            ->integer(count($mod))->isEqualTo(2)
            ->string($mod[0]->getLibelle())->isEqualTo($mod2->getLibelle())
            ->integer(intval($mod[0]->getEnseignantReferant()))->isEqualTo($mod2->getEnseignantReferant())
            ->string($mod[1]->getLibelle())->isEqualTo($mod1->getLibelle())
            ->integer(intval($mod[1]->getEnseignantReferant()))->isEqualTo($mod1->getEnseignantReferant());
    }

    public function  testGetAll(){
        $this->setup();
        $mod1 = new Module("test",1);
        $mod2 = new Module("ijtestou",3);
        $mod3 = new Module("rienavoir",5);
        $mod = array();
        $this->lastModuleId = array();
        $this
            ->given()
            ->and(
                array_push($this->lastModuleId,$this->dao->add($mod3)),
                array_push($this->lastModuleId,$this->dao->add($mod2)),
                array_push($this->lastModuleId,$this->dao->add($mod1)),
                $mod = $this->dao->getAll()
            )
            ->then
            ->integer(count($mod))->isEqualTo(3)
            ->string($mod[0]->getLibelle())->isEqualTo($mod3->getLibelle())
            ->integer(intval($mod[0]->getEnseignantReferant()))->isEqualTo($mod3->getEnseignantReferant())
            ->string($mod[1]->getLibelle())->isEqualTo($mod2->getLibelle())
            ->integer(intval($mod[1]->getEnseignantReferant()))->isEqualTo($mod2->getEnseignantReferant())
            ->string($mod[2]->getLibelle())->isEqualTo($mod1->getLibelle())
            ->integer(intval($mod[2]->getEnseignantReferant()))->isEqualTo($mod1->getEnseignantReferant());
    }

    public function  testUpdate()
    {
        $this->setup();
        $this
            ->given($mod=new Module("thom",1))
            ->and(
                $mod->setIdModule($this->dao->add($mod)),
                $modTmp=new Module("Good",1),
                $modTmp->setIdModule($mod->getIdModule()),
                $this->dao->update($modTmp),
                $updatedModule=$this->dao->getById($modTmp->getIdModule()),
                $this->lastModuleId = $mod->getIdModule()
            )
            ->then
            ->string($modTmp->getLibelle())->isEqualTo($updatedModule->getLibelle())
            ->integer($modTmp->getEnseignantReferant())->isEqualTo($updatedModule->getEnseignantReferant()
            );
    }

    public function testFinUserByIdFail(){
        $this->setup();
        $this
            ->given($mod = $this->dao->getById(-1))
            ->then->variable($mod)->isNull();
    }

    public function testAddModule(){
        $this->setup();
        $this
            ->given($mod=new Module("thoma",1))
            ->and(
                $this->lastModuleId=$this->dao->add($mod),
                $addedModule=$this->dao->getById($this->lastModuleId)
            )
            ->then
            ->string($mod->getLibelle())
                ->isEqualTo($addedModule->getLibelle())
            ->integer($mod->getEnseignantReferant())
                ->isEqualTo($addedModule->getEnseignantReferant());
    }

    public function testGetById() {
        $this->setup();
        $this
            ->given($mod = new Module("thomas",1))
            ->and(
                $this->lastModuleId = $this->dao->add($mod),
                $addedModule = $this->dao->getById($this->lastModuleId)
            )
            ->then
            ->string($mod->getLibelle())
                ->isEqualTo($addedModule->getLibelle())
            ->integer($mod->getEnseignantReferant())
                ->isEqualTo($addedModule->getEnseignantReferant());
    }
}