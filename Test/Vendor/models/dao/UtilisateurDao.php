<?php
namespace Vendor\models\dao\tests\units;

use atoum;

use mageekguy\atoum\asserters\integer;
use Vendor\models\dao\ConnexionToServer;
use Vendor\models\dao\DaoFactory;
use Vendor\models\linkers\EnumRole;
use Vendor\models\linkers\Utilisateur;

/**
 * @engine isolate
 */
class UtilisateurDao extends atoum
{
    private $factory;
    private $db_file_config="Source/Vendor/ressources/base_donnees/bd_connect.ini";
    private $dao;
    private $testTable="testUtilisateur";
    private $dbConnect;
    private $lastUserId;


    public function setUp(){
        $this->dbConnect=new ConnexionToServer($this->db_file_config);
        $this->factory=new DaoFactory($this->dbConnect);
        $this->dao=$this->factory->getInstanceOfUtilisateurDao();
        $this->dao->setTable($this->testTable);
    }

    public function beforeTestMethod($method){
        switch ($method){
            case 'testAddUser':
                break;

        }

    }

    public function afterTestMethod($method){
        switch ($method){
            case 'testAddUser':
                $testedUser = new Utilisateur("tmp","tmp",1);
                $testedUser->setId($this->lastUserId);
                $this->dao->delete($testedUser);
                break;
            case 'testAddUserWithExistingEamil':
                $testedUser = new Utilisateur("tmp","tmp",1);
                $testedUser->setId($this->lastUserId);
                $this->dao->delete($testedUser);
                break;
            case 'testUpdate':
                $user = new Utilisateur("tmp","tmp",1);
                $user->setId($this->lastUserId);
                $this->dao->delete($user);
                break;
            case 'testFindUserById':
                $user = new Utilisateur("tmp","tmp",1);
                $user->setId($this->lastUserId);
                $this->dao->delete($user);
                break;
            case 'testFindByEmail':
                $testedUser = new Utilisateur("tmp","tmp",1);
                $testedUser->setId($this->lastUserId);
                $this->dao->delete($testedUser);
                break;
            case 'testAuthentifyUser':
                $testedUser = new Utilisateur("tmp","tmp",1);
                $testedUser->setId($this->lastUserId);
                $this->dao->delete($testedUser);
                break;
            case 'testExistEmail':
                $testedUser = new Utilisateur("tmp","tmp",1);
                $testedUser->setId($this->lastUserId);
                $this->dao->delete($testedUser);
                break;



        }
    }

    /**
     *
     */
    public function testAddUser(){
        $this->setup();
        $this
            ->given($user=new Utilisateur("thomas","coco",2))
            ->and(
                $user->setMail("thomasss@gmail.com"),
                $user->setMotDePass("pass"),
                $this->lastUserId=$this->dao->add($user),
                $addedUser=$this->dao->getUserById($this->lastUserId)
                )
            ->then
                ->string($user->getNom())->isEqualTo($addedUser->getNom())
                ->string($user->getPrenom())->isEqualTo($addedUser->getprenom())
                ->string($user->getMail())->isEqualTo($addedUser->getMail())
                ->string($user->getMotDePass())->isEqualTo($addedUser->getMotDePass())
                ->integer($user->getRoleModel())->isEqualTo($addedUser->getRoleModel()
            );
    }

    /**
     *
     */
    public function testAddUserWithExistingEamil(){
        $this->setup();
        $this
            ->given($user=new Utilisateur("thomas","coco",2))
            ->and(
                $user->setMail("thomass@gmail.com"),
                $user->setMotDePass("pass"),
                $this->lastUserId=$this->dao->add($user)
            )
            ->then->exception(
                function(){
                    //ajout de l'utilisateur avec le même email
                    $newUser=new Utilisateur("thomas","coco",2);
                    $newUser->setMail("thomass@gmail.com");
                    $this->lastUserId=$this->dao->add($newUser);
                }
            )->hasCode(10);
    }


    /**
     *
     */
    public function testFindUserById(){
        $this->setup();
        $this
            ->given($user=new Utilisateur("Chico","yupi",2))
            ->and(
                $user->setMail("chico@gmail.com"),
                $user->setMotDePass("passpas"),
                $this->lastUserId=$this->dao->add($user),
                $addedUser=$this->dao->getUserById($this->lastUserId)
            )
            ->then
            ->string($user->getNom())->isEqualTo($addedUser->getNom())
            ->string($user->getPrenom())->isEqualTo($addedUser->getprenom())
            ->string($user->getMail())->isEqualTo($addedUser->getMail())
            ->string($user->getMotDePass())->isEqualTo($addedUser->getMotDePass())
            ->string($this->lastUserId)->isEqualTo($addedUser->getId())
            ->integer($user->getRoleModel())->isEqualTo($addedUser->getRoleModel());
    }

    /**
     *
     */
    public function testFinUserByIdFail(){
        $this->setup();
        $this
            ->given($user=$this->dao->getUserById(-1))
            ->then->variable($user)->isNull();
    }

    /**
     *
     */
    public function  testFindByEmail(){
        $this->setup();
        $this
            ->given($user=new Utilisateur("Gringo","tapass",2))
            ->and(
                $user->setMail("tacos@yahoo.com"),
                $user->setMotDePass("passpas"),
                $this->lastUserId=$this->dao->add($user),
                $addedUser=$this->dao->findByEmail("tacos@yahoo.com")
            )
            ->then
            ->string($user->getNom())->isEqualTo($addedUser->getNom())
            ->string($user->getPrenom())->isEqualTo($addedUser->getprenom())
            ->string($user->getMail())->isEqualTo($addedUser->getMail())
            ->string($user->getMotDePass())->isEqualTo($addedUser->getMotDePass())
            ->integer($user->getRoleModel())->isEqualTo($addedUser->getRoleModel());
    }

    /**
     *
     */
    public function  testFindByEmailFail(){
        $this->setup();
        $this
            ->given($user=$this->dao->findByEmail("nul@dontExist.null"))
            ->then->variable($user)->isNull();
    }




    public function  testUpdate()
    {
        $this->setup();
       $this
            ->given($user=new Utilisateur("thomas","coco",2))
            ->and(
                $user->setMail("thom@gmail.com"),
                $user->setMotDePass("pass"),
                $user->setId($this->dao->add($user)),
                $userTmp=new Utilisateur("Good","luck",2),
                $userTmp->setId($user->getId()),
                $userTmp->setMail("superToto@gmail.fr"),
                $userTmp->setMotDePass("newPass"),
                $this->dao->update($userTmp),
                $updatedUser=$this->dao->getUserById($userTmp->getId()),
                $this->lastUserId = $user->getId()
            )
            ->then
            ->string($userTmp->getNom())->isEqualTo($updatedUser->getNom())
            ->string($userTmp->getPrenom())->isEqualTo($updatedUser->getprenom())
            ->string($userTmp->getMail())->isEqualTo($updatedUser->getMail())
            ->string($userTmp->getMotDePass())->isEqualTo($updatedUser->getMotDePass())
            ->integer($userTmp->getRoleModel())->isEqualTo($updatedUser->getRoleModel());
    }


    public function testAuthentifyUser(){
        $this->setup();
        $this
            ->given($user=new Utilisateur("thomas","coco",2))
            ->and(
                $user->setMail("tom@gmail.com"),
                $user->setMotDePass("passPartout"),
                $this->lastUserId=$this->dao->add($user),
                $authentifiedUser=$this->dao->authentifyUser("tom@gmail.com","passPartout")
                )
            ->then
                ->string($user->getNom())->isEqualTo($authentifiedUser->getNom())
                ->string($user->getPrenom())->isEqualTo($authentifiedUser->getprenom())
                ->string($user->getMail())->isEqualTo($authentifiedUser->getMail())
                ->string($user->getMotDePass())->isEqualTo($authentifiedUser->getMotDePass())
                ->integer($user->getRoleModel())->isEqualTo($authentifiedUser->getRoleModel())
            ;


    }

    /**
     *
     */
    public function testAuthentifyUserFail(){
        $this->setup();
        $this
            ->given($user=$this->dao->authentifyUser("nul@dontExist.null","notExist"))
            ->then->variable($user)->isNull();
    }



    public function testExistEmail(){
        $this->setup();
        $this
            ->given($user=new Utilisateur("thomas","coco",2))
            ->and(
                $user->setMail("prout@gmail.com"),
                $user->setMotDePass("capass"),
                $this->lastUserId=$this->dao->add($user)

            )
            ->then->boolean($this->dao->existEmail("prout@gmail.com"))->isTrue();
    }

    public function testExistEmailFail(){
        $this->setup();
        $this->then->boolean($this->dao->existEmail("prouttttt@gmail.com"))->isFalse();
    }



}