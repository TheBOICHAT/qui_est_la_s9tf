<?php
namespace Vendor\models\dao\tests\units;

use atoum;

use Vendor\models\dao\ConnexionToServer;
use Vendor\models\dao\DaoFactory;
use Vendor\models\linkers\Module;
use Vendor\models\dao;

require_once("Source/Vendor/models/linkers/Module.php");
require_once("Source/Vendor/models/dao/ModuleDao.php");
require_once("Source/Vendor/models/dao/UtilisateurDao.php");
require_once("Source/Vendor/models/dao/DaoFactory.php");

/**
 * @engine isolate
 */
class UtilisateurHasModuleDao extends atoum
{
    private $factory;
    private $db_file_config="Source/Vendor/ressources/base_donnees/bd_connect.ini";
    private $modulDao;
    private $uhmDao;
    private $testModule="testModule";
    private $testUtilisateurHasModule="testUtilisateur_has_module";
    private $dbConnect;
    private $lastModuleId;

    public function setUp(){
        $this->dbConnect=new ConnexionToServer($this->db_file_config);
        $this->factory=new DaoFactory($this->dbConnect);
        $this->modulDao = new dao\ModuleDao($this->factory);
        $this->uhmDao=new dao\UtilisateurHasModuleDao($this->factory);
        $this->modulDao->setTable($this->testModule);
        $this->uhmDao->setTable($this->testUtilisateurHasModule);
    }

    public function testLinkUnlink() {
        $this->setUp();
        $mod = new Module("blabla",1);
        $this->lastModuleId = $this->modulDao->add($mod);
        $this->uhmDao->link($this->lastModuleId, 4);
        $row = $this->uhmDao->getAllUtilisateur($this->lastModuleId);
        $this
            ->then
            ->integer(1)->isEqualTo(count($row));

        $this->uhmDao->unlink($this->lastModuleId, 4);
        $testedModule = new Module("",1);
        $testedModule->setIdModule($this->lastModuleId);
        $this->modulDao->delete($testedModule);
    }

    public function testGetAllUtilisateur(){
        $this->setUp();
        $mod = new Module("blabla",1);


        $mod->setIdModule($this->modulDao->add($mod));

        $this->uhmDao->link($mod->getIdModule(), 1);
        $this->uhmDao->link($mod->getIdModule(), 4);
        $this->uhmDao->link($mod->getIdModule(), 5);
        $row = $this->uhmDao->getAllUtilisateur($mod->getIdModule());
        $this
            ->then
            ->integer(3)->isEqualTo(count($row));

        $this->uhmDao->unlink($mod->getIdModule(), 1);
        $this->uhmDao->unlink($mod->getIdModule(), 4);
        $this->uhmDao->unlink($mod->getIdModule(), 5);
        $this->modulDao->delete($mod);
    }

    public function testGetAllModule(){
        $this->setUp();
        $mod = new Module("blabla",1);
        $mod1 = new Module("blabla",1);
        $mod2 = new Module("blabla",1);
        $mod3 = new Module("blabla",1);


        $mod->setIdModule($this->modulDao->add($mod));
        $mod1->setIdModule($this->modulDao->add($mod1));
        $mod2->setIdModule($this->modulDao->add($mod2));
        $mod3->setIdModule($this->modulDao->add($mod3));

        $this->uhmDao->link($mod->getIdModule(), 3);
        $this->uhmDao->link($mod1->getIdModule(), 3);
        $this->uhmDao->link($mod2->getIdModule(), 3);
        $this->uhmDao->link($mod3->getIdModule(), 3);
        $row = $this->uhmDao->getAllModule(3);
        $this
            ->then
            ->integer(4)->isEqualTo(count($row));

        $this->uhmDao->unlink($mod->getIdModule(), 3);
        $this->uhmDao->unlink($mod1->getIdModule(), 3);
        $this->uhmDao->unlink($mod2->getIdModule(), 3);
        $this->uhmDao->unlink($mod3->getIdModule(), 3);
        $this->modulDao->delete($mod);
        $this->modulDao->delete($mod1);
        $this->modulDao->delete($mod2);
        $this->modulDao->delete($mod3);
    }



}