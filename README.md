# qui_est_la_S9TF

M2 S9 project using ... and ... . 
Gestion of missing class student.

Pour lancer les tests en php avec atoum (on lance les tests sur sa machine avant de push sur gitlab le push est rejeté) :

php applis/atoum/atoum.phar -d Test/ -bf Test/.bootstrap.atoum.php

Vous pouvez également, sous Intellij, exécuter les tests unitaires via
View -> Tool Windows -> Maven projects -> Lifecycle -> test 
       
Si vous avez des modifs à faire, voulez tester quelquechose, pensez a créer votre branche au paravant et a checkout dessus : 

1. enregistrez les modifs sur votre branche actuel (si sur master ne faites rien)
2. git checkout -b [nom_de_la_nouvelle_branche]
3. git checkout [nom_de_la_branche]
4. faites vos modif, puis ensuite add * et commit
5. git push origin [nom_de_la_branche]

Et pensez a bouger vos cartes sur jira !

Il est nécessaire d'avoir une BDD MySQL en local (via xampp ou lampp) pour faire tourner le projet (pensez à vous faire une classe de connexion à la BDD personel).

