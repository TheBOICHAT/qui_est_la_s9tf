#!/bin/sh


if [ "$#" -lt 1 ] ; then
	echo "usage :\n\t./run.sh : runs the project on php5 built-in server \(to get it : sudo apt-get php5\)\n\t./run.sh test : launch tests"
fi


if [ "$1" -eq "$1 " ] 2>/dev/null ; then
	php -S localhost:"$1" -t .

	else
		if [ "$1" = 'test' ]; then
			php applis/atoum/atoum.phar -d Test/ -bf Test/.bootstrap.atoum.php
		fi
fi 
 
